<?php
    if(@is_file('../front/core.php'))
    {
        include("../front/core.php");
    }
    else
    {
        include("../httpd.private/front/core.php");
    }
    
    //library("session");

    //-----------------------
    

    $view = get_view_name();
    if(empty($_SESSION["servers"]) && $view !== "connect")
    {
        $view = "connect";
        //header("Location: ".url_path("connect"));
    }

    
    if(is_view_only())
    {
        include_view($view);
        return;
    }

    if($view == '404')
    {
        header("HTTP/1.0 404 Not Found");
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FameShares - admin</title>
    <link rel="icon" type="image/png" href="<?= img_path('icon.png'); ?>">
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic">
    <script>window.base_path = '<?= url_path(''); ?>'; </script>
    
    <!--[if IE]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <?php
        script("jquery-1.11.1.min.js");
        css("bootstrap.css");
        script("bootstrap.min.js");
        //css("bootstrap.datetime.css");
        //script("moment.js");
        //script("bootstrap.datetime.js");
        css("bootstrap-select.min.css");
        script("bootstrap-select.js");

        css("font-awesome.min.css");
        css("animate.min.css");
		css("datepicker3.css");
        css("styles.css");

        script("skyhark.js");
        script("extension.js");
        script("lumino.glyphs.js");
        script("notify.js");
        script("chart.min.js");
        //script("chart-data.js");
        script("easypiechart.js");
        //script("easypiechart-data.js");
        script("bootstrap-datepicker.js");
        script("bootbox.js");
    ?>
    
    
</head>
<body>
    <?php

        include_view($view);

    ?>
</body>
</html>