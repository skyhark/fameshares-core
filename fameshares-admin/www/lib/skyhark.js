function init_links(context)
{
    $("a[data-href]", context).click(function(e)
    {
        e.preventDefault();

        var page_link = $(this).attr("data-href");
        var page_name = page_link;

        if(page_name.indexOf("/") != -1)
        {
            page_name = page_name.substr(0, page_name.indexOf("/"));
        }

        var page_title = $(this).attr("data-title") || page_name;
        $("section .content-heading").text(page_title);
        
        if( $(this).parents(".submenu").length == 1) //Sub menu navigation
        {
            show_page(page_link, page_name, "#submenu-content");
        }
        else //Main Menu Navigation
        {
            show_page(page_link, page_name);
        }
        
        
        if($(this).parent().get(0).tagName.toLowerCase() == 'li' && $(this).parents('.submenu').length == 0)
        {
            $("ul.nav li").removeClass("active");
            $(this).parent().addClass("active");
            $("ul.nav li.dropdown").removeClass("open");
        }

        return false;
    });
}

function show_page(page, name, selector)
{    
    var post = {additional: "no-menu"};
    
    if(selector == undefined)
    {
        selector = "#main-content";
    }
    else
    {
        post.submenu = "hidden";
    }
    
    $.post(page, post, function(data)
    {
        $(selector).css("opacity", 0);
        $(selector).html(data);
        $(selector).animate({opacity: 1}, 300);
        init_links($(selector).first());


        if(window.scrollTo)
        {
            window.scrollTo(0, 0);
        }

        if(history)
        {
            if(page == "index")
            {
                history.pushState({}, "SkyHark", "./");
                return;
            }

            history.pushState({}, "SkyHark - " + name, page);
        }
    });
}

$(document).ready(function()
{
    init_links("body");
});



(function ( $ ) {
 
    function force_json(data)
    {
        if(typeof(data) == "string")
        {
            try
            {
                return JSON.parse(data);
            } catch(e)
            {
               return { 'error': data };
            }
        }
        
        return data;
    }
    
    $.fn.sky_form = function(cb_before, cb_after)
    {
        if(cb_after == null)
        {
            cb_after = cb_before;
            cb_before = null;
        }
        
        this.submit(function(e)
        {
            e.preventDefault();
            
            var $post = {additional: 'no-menu'};            
            
            $("input, select, textarea", this).each(function(key, value)
            {
                var name = $(value).attr("name");
                if(name)
                {
                    $post[name] = $(value).val();
                }
            });
            
            var target = $(this).attr("action");
            
            if(cb_before != null)
            {
                if( cb_before.call(this, $post, target) === false)
                {
                    return false;
                }
            }
            
            var that = this;
            $.post(target, $post, function(data)
            {
                data = force_json(data);
                cb_after.call(that, data);
            });
            
            return false;
        });
        
        return this;
    };
    
    
    $.fn.sky_delete = function(cb_before, cb_after, selector)
    {
        if(cb_after == null)
        {
            cb_after = cb_before;
            cb_before = null;
        }
        
        if(!selector)
        {
            selector = "tr";
        }
        
        $(this).click(function(e)
        {
            e.preventDefault();
            var id = $(this).attr("data-id") || $(this).parents("form:first").find("input[name='id']").val();
            var target = $(this).attr("data-href");
            
            if(cb_before != null)
            {
                if( cb_before.call(this, id, target) === false)
                {
                    return false;
                }
            }
            
            if(!id || !target)
            {
                $(this).parents(selector+":first").remove();
                cb_after();
                return;
            }
            
            var that = this;
            $.post(target, {additional: 'no-menu', delete: id}, function(data)
            {
                data = force_json(data);
                
                if(!data.error)
                {
                    $(that).parents(selector+":first").remove();
                }
                
                cb_after.call(that, data);
            });
            
            return false;
        });
        
        return this;
    }
 
}( jQuery ));