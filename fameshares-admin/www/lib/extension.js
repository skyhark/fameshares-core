function get_error(data)
{
    if(typeof(data.error) == "object" && data.error != null)
    {
        return data.error.message;
    }   
    
    return false;
}

$.cli = function(command, args, cb)
{
    var post =
    {
        additional: 'no-menu',
        url: "http://127.0.0.1:1451", 
        user: "user",
        password: "test",
        command: command, 
        params: args
    };
    
    $.post(window.base_path+"/terminal", post, function(data)
    {
        try
        {
            data = JSON.parse(data);
        }
        catch(e)
        {
            data = { error: {code: 0, message: data } };
        }
            
        cb(data, get_error(data));
    }); 
}

$.fn.multi_field = function()
{   
    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + s4() + s4() + s4() + s4();
    }

    function on_leave()
    {
        var fields = $("input[mutli_field_id='" + $(this).attr("mutli_field_id") + "']");
        var empty_fields = fields.filter(function() { return $(this).val() == ""; });

        if(fields.first().filter(function() { return $(this).val() == ""; }).length == 1)
        {
            empty_fields.not(":first").not(":focus").parents(".form-group").remove();
        }
        else
        {
            empty_fields.not(":last").not(":focus").parents(".form-group").remove();
        }


        if(empty_fields.length == 0)
        {
            var nfield = fields.first().parents(".form-group").first().clone();
            var o = fields.last().parents(".form-group").first().after( nfield );
            nfield.find("label").text("");
            nfield.find("input").val("").keyup(on_leave).change(on_leave);
        }
    }

    var _guid = guid();
    this.attr("mutli_field_id", _guid);
    this.find("input").attr("mutli_field_id", _guid);
    this.keyup(on_leave);
    this.find("input").keyup(on_leave).change(on_leave);
};
    

$.fn.having_text = function()
{
    return this.filter(function() { return $(this).val() != ""; });
}


function padLeft(nr, n, str){
    return Array(n-String(nr).length+1).join(str||'0')+nr;
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
