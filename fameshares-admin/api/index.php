<?php
    include("core/core.php");
    header("content-type: text/json");
    header("Access-Control-Allow-Origin: *");

    global $request_path;
    $category = $request_path;

    if(($index = strpos($category, '/')))
    {
        $action   = substr($category, $index+1);
        $category = substr($category, 0, $index);
        
        $result = api($category, $action);
        print json_encode($result);
        return;
    }

    print json_encode(array("error" => "wrong api path"));
?>