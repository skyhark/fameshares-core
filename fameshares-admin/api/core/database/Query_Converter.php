<?php

    class Query_Converter
    {
        var $query;
        var $convertion;
        
        function __construct($query, $convertions)
        {
            $this->query = $query;
            $this->convertion = $convertions;
        }
        
        public function __call($method, $arguments) {
            if (method_exists($this->query, $method)) {
                return call_user_func_array(
                    array($this->query, $method),
                    $arguments
                );
            }
            
            throw new exception("method '$method' not found");
        }
        
        //------------------------------------------------------------*/
        
        
        function field($i)
        {
            $x = 0;
            foreach($this->convertion as $val => $b)
            {
                if($x == $i)
                {
                    return $val;
                }
                $x++;
            }
            
            return false;
        }
        
        function fieldsCount()
        {
            return count( $this->convertion );
        }
        
        function fetchObject()
        {
            if($array = $this->fetchArray())
            {
                foreach($array as $key => $val)
                {
                    if(is_numeric($key))
                    {
                        unset($array[$key]);
                    }
                }

                return json_decode(json_encode($array), FALSE);
            }
        }
        
        function fetchArray()
        {
            if($array = $this->query->fetchArray())
            {
                $last_index = -1;
                $array = (array) $array;
                
                foreach($array as $key => $val)
                {
                    if(!is_numeric($key))
                    {
                        unset($array[$key]);
                        
                        foreach($this->convertion as $key2 => $val2)
                        {
                            if($val2 == $key)
                            {
                                $array[$key2] = $val;
                                break;
                            }
                        }
                    }
                    else if($key > $last_index)
                    {
                        $last_index = $key;
                    }
                }
                
                foreach($this->convertion as $key => $val)
                {
                    if(!array_key_exists($key, $array))
                    {
                        $last_index++;
                        
                        if(is_callable($val))
                        {
                            $val = $val($this);
                        }
                        
                        $array[$key] = $val;
                        $array[$last_index] = $val;
                    }
                }
                
                return $array;
            }
        }     
        
        function fetchRow($index=null)
        {
            if($arr = $this->fetchArray())
            {
                $arr2 = array();

                foreach($arr as $key => $val)
                {
                    if(is_numeric($key))
                    {
                        $arr2[] = $val;
                    }
                }

                return $arr2;
            }
        }
        
        function getAll()
        {
            $result = array();

            while(($row = $this->fetchObject()))
            {
                $result[] = $row;
            }
            
            return $result;
        }
        
        function getColumns()
        {
            $fields = array();
            $length = $this->fieldsCount();
            
            for($i=0; $i<$length; $i++)
            {
                $fields[] = $this->field($i);
            }
            
            return $fields;   
        }
            
    }

?>