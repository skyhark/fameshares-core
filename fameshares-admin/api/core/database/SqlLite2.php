<?php
    class _SqlLite2
    {
        var $link;
        var $name;
        
        function __construct($file)
        {
            $this->name = basename($file);
            /*if(!function_exists("SQLiteDatabase "))
            {
                throw new Exception('SQLiteDatabase not declared');
            }
            else
            {*/
                $this->link = new SQLiteDatabase($file);
           // }
        }
        
        function error()
        {
            return $this->link->lastError();
        }
        
        function getLink()
        {
            return $this->link;   
        }
        
        function connected()
        {
            return mysqli_connect_errno($this->link) ? false : true;  
        }
        
        function connect_error()
        {
            return mysqli_connect_errno($this->link);
        }
        
        function set_database($database)
        {
            
        }
        
        function escape($string)
        {
            return sqlite_escape_string ($string);
        }
        
        function query($query)
        {
            return new _SqlLite2_Result( $this->link->query($query) , $this->link);   
        }
        
        function multi_query($query)
        {
            return $this->link->queryExec($query);
        }
        
        function affected_rows()
        {
            return mysqli_affected_rows($this->link);
        }
        
        function insert_id()
        {
            return $this->link->lastInsertRowid();   
        }
        
        function database()
        {
            return $this->name;
        }
        
        function databases()
        {
            return array($this->name);
        }
        
        function tables($database)
        {
            return $this->json_query("SHOW TABLES FROM $database", 0);
        }
        
        function json_query($query, $field=null)
        {
            if(is_string($query))
            {
                $query = $this->query($query);   
            }
            
            $result = array();
            
            if(!is_numeric($field))
            {
                while(($row = $query->fetchObject()))
                {
                    $result[] = ($field == null) ? $row : $row->$field;
                }   
            }
            else
            {
                while(($row = $query->fetchRow()))
                {
                    $result[] = $row[$field];
                }   
            }
            
            return $result;
        }
        
        function close()
        {
            @sqlite_close($this->link);
        }
    }

    class _SqlLite2_Result
    {
        var $dbRes, $link, $fetchLimits = null;
        
        function __construct($query_result, $_link)
        {
            $this->dbRes = $query_result;
            $this->link = $_link;
        }
        
        function hasErrors()
        {
            return ($dbRes == false);
        }
        
        function error()
        {
            return $this->link->lastError();
        }
        
        function field($i)
        {
            return $this->dbRes->fieldName($i);
        }
        
        function fieldsCount()
        {
            return $this->dbRes->numFields();
        }
        
        function fetchObject()
        {
            if($this->fetchLimits != 0 || is_null($this->fetchLimits))
            {
                $this->fetchLimits = ($this->fetchLimits == null) ? null : $this->fetchLimits-1;
                return sqlite_fetch_object ($this->dbRes);
            }
        }
        
        function fetchArray()
        {
            if($this->fetchLimits != 0 || is_null($this->fetchLimits))
            {
                $this->fetchLimits = ($this->fetchLimits == null) ? null : $this->fetchLimits-1;
                return sqlite_fetch_array ($this->dbRes);
            }
        }     
        
        function fetchRow($index=null)
        {
            $arr = $this->fetchArray();
            $arr2 = array();
            
            foreach($arr as $val)
            {
                $arr2[] = $val;
            }
            
            return $arr2;
        }
        
        function getAll()
        {
            $result = array();

            while(($row = $this->fetchObject()))
            {
                $result[] = $row;
            }
            
            return $result;
        }
        
        function getColumns()
        {
            $fields = array();
            $length = $this->fieldsCount();
            
            for($i=0; $i<$length; $i++)
            {
                $fields[] = $this->field($i);
            }
            
            return $fields;   
        }
        
        function count()
        {
            return sqlite_num_rows ($this->dbRes);
        }
        
        function setLimits($start, $limit)
        {
            
            //    print $this->error();
            
            if($this->dbRes)
            {
                $this->fetchLimits = $limit;
                $this->dbRes->data_seek($start);
            }
        }
    }
?>