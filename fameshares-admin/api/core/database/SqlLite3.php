<?php
    class _SqlLite3
    {
        var $link;
        var $name;
        
        function __construct($file)
        {
            $this->name = basename($file);
            /*if(!function_exists("SQLiteDatabase "))
            {
                throw new Exception('SQLiteDatabase not declared');
            }
            else
            {*/
                $this->link = new SQLite3($file);
           // }
        }
        
        function error()
        {
            return $this->link->lastErrorMsg();
        }
        
        function has_error()
        {
            return $this->link->lastErrorCode() != 0;
        }
        
        function getLink()
        {
            return $this->link;   
        }
        
        function connected()
        {
            return $this->link->lastErrorCode() ? false : true;  
        }
        
        function connect_error()
        {
            return $this->error();
        }
        
        function set_database($database)
        {
            
        }
        
        function escape($string)
        {
            return $this->link->escapeString ($string);
        }
        
        function query($query)
        {
            $q = $this->link->query($query);
            return new _SqlLite3_Result( $q , $this->link);   
        }
        
        function multi_query($query)
        {
            return $this->link->exec($query);
        }
        
        function affected_rows()
        {
            return $this->link->changes();
        }
        
        function insert_id()
        {
            return $this->link->lastInsertRowID();   
        }
        
        function database()
        {
            return $this->name;
        }
        
        function databases()
        {
            return array($this->name);
        }
        
        function tables()
        {
            return $this->json_query("SELECT name FROM sqlite_master WHERE type='table';", 0);
        }
        
        function table_structure($table)
        {
            $query = $this->query("PRAGMA table_info('". $this->escape($table) ."');");

            return new Query_Converter( $query, array
                                        (
                                            "id" => "cid",
                                            "name" => "name",
                                            "type" => "type",
                                            "nullable" => "notnull",
                                            "default" => "dflt_value",
                                            "is_primary" => "pk",
                                            "maximum_length" => "",
                                            "collation" => ""
                                        )
                                    );
        }
        
        function json_query($query, $field=null)
        {
            if(is_string($query))
            {
                $query = $this->query($query);   
            }
            
            $result = array();
            
            if(!is_numeric($field))
            {
                while(($row = $query->fetchObject()))
                {
                    $result[] = ($field == null) ? $row : $row->$field;
                }   
            }
            else
            {
                while(($row = $query->fetchRow()))
                {
                    $result[] = $row[$field];
                }   
            }
            
            return $result;
        }
        
        function close()
        {
            $this->link->close();
        }
    }

    class _SqlLite3_Result
    {
        var $dbRes, $link, $fetchLimits = null;
        
        function __construct($query_result, $_link)
        {
            $this->dbRes = $query_result;
            $this->link = $_link;
        }
        
        function __destruct()
        {
            if(!$this->hasErrors())
            {
                $this->dbRes->finalize ();
            }
        }
        
        function hasErrors()
        {
            return ($this->dbRes == false);
        }
        
        function error()
        {
            return $this->link->lastErrorMsg();
        }
        
        function field($i)
        {
            return $this->dbRes->columnName($i);
        }
        
        function fieldsCount()
        {
            return $this->dbRes->numColumns();
        }
        
        function fetchObject()
        {
            if($this->fetchLimits != 0 || is_null($this->fetchLimits))
            {
                $array = $this->fetchArray();
                
                foreach($array as $key => $val)
                {
                    if(is_numeric($key))
                    {
                        unset($array[$key]);
                    }
                }
                
                return json_decode(json_encode($array), FALSE);
            }
        }
        
        function fetchArray()
        {
            if(!$this->dbRes)
            {
                return false;
            }
            if($this->fetchLimits != 0 || is_null($this->fetchLimits))
            {
                $this->fetchLimits = ($this->fetchLimits == null) ? null : $this->fetchLimits-1;
                return $this->dbRes->fetchArray();
            }
        }     
        
        function fetchRow($index=null)
        {
            if($arr = $this->fetchArray())
            {
                $arr2 = array();

                foreach($arr as $key => $val)
                {
                    if(is_numeric($key))
                    {
                        $arr2[] = $val;
                    }
                }

                return $arr2;
            }
        }
        
        function getAll()
        {
            $result = array();

            while(($row = $this->fetchObject()))
            {
                $result[] = $row;
            }
            
            return $result;
        }
        
        function getColumns()
        {
            $fields = array();
            $length = $this->fieldsCount();
            
            for($i=0; $i<$length; $i++)
            {
                $fields[] = $this->field($i);
            }
            
            return $fields;   
        }
        
        function count()
        {
            return sqlite_num_rows ($this->dbRes);
        }
        
        function setLimits($start, $limit)
        {
           /* if($this->dbRes)
            {
                $this->fetchLimits = $limit;
                $this->dbRes->data_seek($start);
            }*/
        }
    }
?>