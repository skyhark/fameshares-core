<?php
    include(dirname(__FILE__)."/Mysql.php");
    include(dirname(__FILE__)."/Mysqli.php");
    include(dirname(__FILE__)."/Postgre.php");
    include(dirname(__FILE__)."/SqlLite2.php");
    include(dirname(__FILE__)."/SqlLite3.php");

    class Database extends Extendable
    {
        var $auto_close;
        
        function __construct($server=null, $user=null, $pass=null, $database=null, $type=null) {
            $this->auto_close = true;
            if(!isset($server))
            {
                global $manager_config;
                $manager_config = isset($manager_config) ? $manager_config : (($_SERVER["SERVER_NAME"] == "localhost") ? getConfig("Manager_local") : getConfig("Manager"));

                $config = $manager_config;
                $server = getProperty($config, "server", "localhost");
                $user   = getProperty($config, "user", "root");
                $pass   = getProperty($config, "password", "");
                $database = getProperty($config, "database");
                $type = getProperty($config, "db_type", "mysql");
            }
            else if(strtolower($user) == 'sqllite' && $pass == null && $database == null && $type == null)
            {
                $type = 'sqllite';
            }
            else if(!isset($type))
            {
                $config = getConfig("Manager");
                $type = getProperty($config, "db_type", "mysql");
            }

            switch( strtolower($type) )
            {
                case 'mysql':
                    $link = new _Mysql($server, $user, $pass, $database);
                    break;
                case 'mysqli':
                    $link = new _Mysqli($server, $user, $pass, $database);
                    break;
                case 'postgre':
                    $link = new _Postgre($server, $user, $pass, $database);
                    break;
                case 'sqllite2':
                    $link = new _SqlLite2($server);
                case 'sqllite3':
                case 'sqllite':
                    $link = new _SqlLite3($server);
                    break;
            }

            $this->registerHandler($link);
        }
        
        function __destruct()
        {
            if(!$this->auto_close)
            {
                return;
            }
            
            $this->close();
            
            global $_DATABASE;
            
            if(!is_array($_DATABASE))
            {
                return;
            }
            
            foreach($_DATABASE as $key => $db)
            {
                //print "descrut $key<br>";
                if($db == $this)
                {
                    unset($_DATABASE[$key]);
                    break;
                }
            }
        }
        
        function csv_query($sql, $settings=array())
        {
            //Headers
            //Handlers
            
            $query = $this->query($sql);
            $header="";
            $data = "";

            if(isset($settings["headers"]))
            {
                foreach($settings["headers"] as $head)
                {
                    $header .= $head . "\t";
                }
            }
            else
            {
                $fields = $query->fieldsCount();
                for ( $i = 0; $i < $fields; $i++ )
                {
                    $header .= $query->field($i) . "\t";
                }
            }

            $handlers = getProperty($settings, "handlers", array());
            while( $row = $query->fetchRow() )
            {
                $line = '';
                $i = 0;
                foreach( $row as $value )
                {
                    if(isset($handlers[$i]))
                    {
                        $value = $handlers[$i]($value);
                    }
                    else
                    {
                        $value = csv_coll($value);
                    }
                    $line .= $value;
                    
                    $i ++;
                }
                $data .= trim( $line ) . "\n";
            }
            $data = str_replace( "\r" , "" , $data );

            if ( $data == "" )
            {
                $data = "\n(0) Records Found!\n";                        
            }
            
            return "$header\n$data";
        }
    }

    function csv_coll($value)
    {
        if ( ( !isset( $value ) ) || ( $value == "" ) )
        {
            $value = "\t";
        }
        else
        {
            $value = str_replace( '"' , '""' , $value );
            $value = '"' . $value . '"' . "\t";
        }
        
        return $value;
    }

   /* html("test");
    $obj = new Database();
    print $obj->connected() ? "true" : "error: ".$obj->error();
    print "<br>";
    $dbRes = $obj->query("SELECT * FROM users") or die($obj->error());
    print $dbRes->count();
    
    _print_r( $dbRes->getAll() );

    exit;*/
?>