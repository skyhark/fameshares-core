<?php
    class _Mysqli
    {
        var $link;
        var $closed;
        
        function __construct($server, $user, $pass, $database=null)
        {
            $this->closed = false;
            
            if(!function_exists("mysqli_connect"))
            {
                throw new Exception('mysqli_connect not declared');
            }
            else
            {
                if(($pos = strpos($server, ":")))
                {
                    $port = substr($server, $pos+1);
                    $server = substr($server, 0, $pos);
                    //print "Server: $server<br>Port: $port<br>";
                    $this->link = @mysqli_connect($server, $user, $pass, null, 35111);
                }
                else
                {
                    $this->link = @mysqli_connect($server, $user, $pass);
                }

                if(isset($database) && $this->connected())
                {
                   $this->set_database($database);
                }
            }
        }
        
        function error()
        {
            if(!$this->connected())
            {
                return $this->connect_error();
            }
            return mysqli_error($this->link);
        }
        
        function getLink()
        {
            return $this->link;   
        }
        
        function connected()
        {
            return $this->closed ? false : (mysqli_connect_errno($this->link) ? false : true);  
        }
        
        function connect_error()
        {
            return mysqli_connect_error ($this->link);
        }
        
        function set_database($database)
        {
            mysqli_select_db($this->link, $database);
        }
        
        function escape($string)
        {
            return @mysqli_real_escape_string($this->link, $string);
        }
        
        function query($query)
        {
            return new _Mysqli_Result(@mysqli_query($this->link, $query), $this->link);   
        }
        
        function multi_query($query)
        {
            return $this->link->multi_query($query);
        }
        
        function affected_rows()
        {
            return @mysqli_affected_rows($this->link);
        }
        
        function insert_id()
        {
            return mysqli_insert_id($this->link);   
        }
        
        function database()
        {
            $dbRes = $this->query("SELECT DATABASE()");
            return $dbRes->fetchRow(0);
        }
        
        function databases()
        {
            return $this->json_query("SHOW DATABASES", 0);
        }
        
        function tables($database)
        {
            return $this->json_query("SHOW TABLES FROM $database", 0);
        }
        
        function json_query($query, $field=null)
        {
            if(is_string($query))
            {
                $query = $this->query($query);   
            }
            
            $result = array();
            
            if(!is_numeric($field))
            {
                while(($row = $query->fetchObject()))
                {
                    $result[] = ($field == null) ? $row : $row->$field;
                }   
            }
            else
            {
                while(($row = $query->fetchRow()))
                {
                    $result[] = $row[$field];
                }   
            }
            
            return $result;
        }
        
        function close()
        {
            $this->closed = true;
            @mysqli_close($this->link);
        }
    }

    class _Mysqli_Result
    {
        var $dbRes, $link, $fields, $fetchLimits = null;
        
        function __construct($query_result, $_link)
        {
            $this->dbRes = $query_result;
            $this->link = $_link;
        }
        
        function hasErrors()
        {
            return ($dbRes == false);
        }
        
        function error()
        {
            return mysqli_error($this->link);
        }
        
        function field($i)
        {
            if($this->fields == null)
            {
                $this->fields = $this->dbRes->fetch_fields();
            }

            return isset($this->fields[$i]) ? $this->fields[$i]->name : null;
        }
        
        function fieldsCount()
        {
            if($this->fields == null)
            {
                $this->fields = $this->dbRes->fetch_fields();
            }

            return count($this->fields);
        }
        
        function fetchObject()
        {
            if($this->fetchLimits != 0 || is_null($this->fetchLimits))
            {
                $this->fetchLimits = ($this->fetchLimits == null) ? null : $this->fetchLimits-1;
                return @mysqli_fetch_object($this->dbRes);
            }
        }
        
        function fetchArray()
        {
            if($this->fetchLimits != 0 || is_null($this->fetchLimits))
            {
                $this->fetchLimits = ($this->fetchLimits == null) ? null : $this->fetchLimits-1;
                return @mysqli_fetch_array($this->dbRes);
            }
        }     
        
        function fetchRow($index=null)
        {
            if($this->fetchLimits != 0 || is_null($this->fetchLimits))
            {
                $this->fetchLimits = ($this->fetchLimits == null) ? null : $this->fetchLimits-1;
                $row = @mysqli_fetch_row($this->dbRes);
                return is_numeric($index) ? $row[$index] : $row;
            }
        }
        
        function getAll()
        {
            $result = array();

            while(($row = $this->fetchObject()))
            {
                $result[] = $row;
            }
            
            return $result;
        }
        
        function getColumns()
        {
            return mysqli_fetch_fields($this->dbRes);   
        }
        
        function count()
        {
            return @mysqli_num_rows($this->dbRes);
        }
        
        function setLimits($start, $limit)
        {
            
            //    print $this->error();
            
            if($this->dbRes)
            {
                $this->fetchLimits = $limit;
                $this->dbRes->data_seek($start);
            }
        }
    }
?>