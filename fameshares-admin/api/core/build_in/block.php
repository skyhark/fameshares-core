<?php

    function cli_result($cmd, $params, $default = '')
    {
        $content = (array) auto_cli($cmd, $params);
        return getProperty($content, 'result', $default);
    }

    function blocks_count($action_id)
    {
        return cli_result('getblockcount', array($action_id));
    }

    function balance($action_id)
    {
        return cli_result('getbalance', array($action_id));
    }

    function block($action_id, $hash)
    {
        return cli_result('getblock', array($action_id, $hash));
    }

    function block_from_index($action_id, $index)
    {
        $hash = cli_result('getblockhash', array($action_id, $index));
        return block($action_id, $hash);
    }

?>