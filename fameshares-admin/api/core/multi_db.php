<?php

    class multi_database
    {
        var $databases = array();
        
        function __construct($types)
        {

            $this->databases = $types;
            
            //$this->registerHandler($link);
        }
        
        function __destruct()
        {
            global $_DATABASE;
            
            if(!is_array($_DATABASE))
            {
                return;
            }
            
            foreach($_DATABASE as $key => $db)
            {
                if($db == $this)
                {
                    unset($_DATABASE[$key]);
                    break;
                }
            }
        }
        
    
        public function __call($method, $arguments) {
            $result = null;
            
            foreach ($this->databases as $type) {
                $db = db( $type );
                
                if(!$db->connected())
                {
                    return false;
                }
                
                $result = call_user_func_array(
                        array($db, $method),
                        $arguments
                    );
                
            }
            
            return $result;
        }
    }
    

?>