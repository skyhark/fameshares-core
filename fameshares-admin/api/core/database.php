<?php
    include_once(dirname(__FILE__).'/variables.php');
    include_once(dirname(__FILE__).'/extendable.php');
    include_api_dir("database/*.php");

    function db($type = null)
    {
        if($type == null)
        {
            global $active_db;
            
            if(!isset($active_db))
            {
                $active_db = "default";
            }
            $type = $active_db;
        }

        global $_DATABASE;
        
        if(!isset($_DATABASE))
        {
            $_DATABASE = array();
        }
        
        $typex = $type; 
        if(is_array($typex))
        {
            $typex = join($typex, '_');
        }
     
        //-------------------------------
        
        if(!isset($_DATABASE[$typex]))
        {
            //print "create $typex<br>";
            if(is_array($type))
            {
                global $_DATABASE;
                $_DATABASE[$typex] = new multi_database($type);
            }
            else
            {
                $config = getConfig("servers"); //local or not ?
                if(!isset($config[$type]))
                {
                    return null;
                }
                
                $config = (array) $config[$type];
                $server = getProperty($config, 'server', '');
                $user   = getProperty($config, 'user', '');
                $pass   = getProperty($config, 'password', '');
                $type  = getProperty($config, 'type', 'mysqli');
                $db     = getProperty($config, 'database', '');

                global $_DATABASE;
                $_DATABASE[$typex] = new Database($server, $user, $pass, $db, $type);
            }
        }
        
        return $_DATABASE[$typex];
    }

    function use_db()
    {
        global $active_db;
        $active_db = func_get_args();
        
        if(count($active_db) == 1)
        {
            $active_db = $active_db[0];
        }
        
        //return false;
        return check_connection();
    }

    function prepare_query($sql, $vars = null, $db_type = null)
    {
        if($vars == null)
        {
            $vars = $_POST;
        }
        
        $db = db( $db_type );
        
        $index = 0;
        while(($index = @strpos($sql, '{@', $index)))
        {
            if(($index2 = strpos($sql, '}', $index)))
            {
                $name = substr($sql, $index+2, $index2 - $index-2);
                if(strpos($name, ' ') === false)
                {
                    $val = getProperty($vars, $name, '');
                    $val = $db->escape($val);
                    $sql = str_replace('{@'.$name.'}', $val, $sql);
                }
            }
            
            $index++;
        }
        
        return $sql;
    }

    function query($sql, $vars = null, $db_type = null)
    {
        $db = db( $db_type );
        $sql = prepare_query($sql, $vars, $db_type);
        return $db->query($sql);
    }

    function query_fetch($sql, $vars = null, $default=0, $type = null)
    {
        if(is_string($vars))
        {
            $type = $vars;
            $vars = $_POST;
        }
        else if($vars == null)
        {
            $vars = $_POST;
        }
        
        $query = query($sql, $vars);
        
        if(($row = $query->fetchRow()))
        {
            return $row[0];
        }
        
        return $default;
    }

    function query_object($sql, $vars = null, $default = 0, $type = null)
    {
        if(is_string($vars))
        {
            $type = $vars;
            $vars = $_POST;
        }
        else if($vars == null)
        {
            $vars = $_POST;
        }
        
        $query = query($sql, $vars);
        
        if(($row = $query->fetchObject()))
        {
            return $row;
        }
        
        return $default;
    }

    function check_connection($type = null)
    {
        $db = db($type);
        
        if($db == null)
        {
            api_result(array("error" => "Connection to the database fails", "db_error" => true));
            return false;
        }
        else if(!$db->connected())
        {
           /* if($type == null)
            {
                global $active_db;
                $type = $active_db;
            }*/
            
            api_result(array("error" => "Connection to the database fails", "db_error" => true));
            return false;
        }
        
        return true;
    }

?>