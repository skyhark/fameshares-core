<?php

    $content = (array) auto_cli('getaddednodeinfo', array(true));
    $content = (array) getProperty($content, 'result', array());

    $result = array();
    foreach($content as $val)
    {
        $addr = (array)getProperty((array) $val, 'addresses', array());
        $addresses = "";
        foreach($addr as $ad) 
        {
            if(!empty($addresses))
            {
                $addresses .= ", ";
            }
            
            $addresses .= getProperty((array)$ad, 'address', 'error');
        }
        
        $result[] = array
        (
            "ip" => getProperty((array) $val, 'addednode', 'Error'),
            "connected" => getProperty((array) $val, 'connected', false) ? "true" : "false",
            "addresses" => $addresses
        );
    }

    api_result($result);
?>