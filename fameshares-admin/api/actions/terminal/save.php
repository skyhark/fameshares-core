<?php

    $_POST['params_p'] = json_encode($_POST['params']);
    $_POST['review'] = ($_POST['review'] == 'true') ? '1' : '0';

    if($_POST['id'] == -1)
    {
        return api_query("INSERT INTO `terminal_actions` (`id`, `name`, `server`, `user`, `password`, `command`, `params`, `review`, `comment`) VALUES ".
                         "(NULL, '{@name}', '{@url}', '{@user}', '{@password}', '{@command}', '{@params_p}', '{@review}', '{@comment}');");
    }

    api_query("UPDATE terminal_actions SET name='{@name}', server='{@url}', user='{@user}', password='{@password}', command='{@command}', params='{@params_p}', review='{@review}', comment='{@comment}' WHERE id = '{@id}'");

?>