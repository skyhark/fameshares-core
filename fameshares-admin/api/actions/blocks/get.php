<?php

    $content = (array) auto_cli('getblock', array($_POST['identifier'], $_POST['hash']));
    $result = (array) getProperty($content, 'result', array());
    $height = getProperty($result, 'height', 0);

    if($height == '0')
    {
        $result['prev_hash'] = '0000000000000000000000000000000000000000000000000000000000000000';
    }
    else
    {
        $content = block_from_index($_POST['identifier'], $height-1);
        $result['prev_hash'] = getProperty($content, 'hash', 'Error');
    }

    $next = block_from_index($_POST['identifier'], $height+1);
    $result['next_hash'] = getProperty($content, 'hash', '');

    api_result($result);

?>