<?php
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);

    date_default_timezone_set('UTC');

    global $request_path_f;
    $request_path_f = $_SERVER['REQUEST_URI'];

    if(($x = strrpos ($_SERVER['SCRIPT_NAME'], '/')))
    {
        $request_path_f = substr($request_path_f, $x+1);
    }

    if(substr($request_path_f, 0, 1) == '/')
    {
        $request_path_f = substr($request_path_f, 1);
    }

	function include_dir($reg)
	{
        $platform_path = dirname(__FILE__).'/';
        $arr = (array) glob($platform_path.$reg);
		foreach ($arr as $filename) {
			if($filename != $platform_path."core/Core.php" && !empty($filename))
			{
				include_once($filename);
				$GLOBALS += get_defined_vars();
			}
		}
	}
	
	include_dir("core/*.php");
?>