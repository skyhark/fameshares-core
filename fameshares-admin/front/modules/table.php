<table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
    <thead>
    <tr>
        <?php
            foreach($settings['colls'] as $name)
            {
                print '<th>'.$name.'</th>';
            }
        
            if(isset($settings['actions']))
            {
                print '<th></th>';
            }
            if(getProperty($settings, 'removable', false))
            {
                print '<th style="width: 50px"></th>';
            }
        ?>
    </tr>
    </thead>
    <tbody>
        
        <?php
            foreach((array)$settings['data'] as $row)
            {
                $row = (array)$row;
                print '<tr>';
                
                foreach($row as $nv)
                {
                    if(is_bool($nv))
                    {
                        $nv = $nv ? 'true' : 'false';
                    }
                    print '<td>'.$nv.'</td>';
                }
                
                if(isset($settings['actions']))
                {
                    ?>
                    <td style="width: 50px">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">...
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <?php
                                    $name = get_view_name().'/'.get_subview();
                                        
                                    foreach($settings['actions'] as $key)
                                    {
                                        $row['paged_action'] = $key;
                                        $data = json_encode($row);
                                        print '<li><a href="" onclick=\'return table_action("'.$name.'", '.$data.')\'>'.$key.'</a></li>';
                                    }
                                ?>
                            </ul>
                        </div>
                    </td>
                    <?php
                }
                
                if(getProperty($settings, 'removable', false))
                {
                    $name = get_view_name().'/'.get_subview();
                    $row['paged_action'] = "remove";
                    $data = json_encode($row);
                    print '<td><button type="button" class="btn btn-danger" onclick=\'return table_action("'.$name.'", '.$data.')\'>Remove</button>';
                }
                
                print '</tr>';
            }
        ?>
    </tbody>
</table>


<script>

    function table_action(page, data)
    {
        data.additional = 'no-menu';
        
        $.post(base_path + page, data, function(cb)
        {
            console.log(cb);
            alert("Done");
            location.href = base_path + page;
        });
        
        return false;
    }

</script>
