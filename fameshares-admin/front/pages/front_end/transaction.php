<?php
    $parts = get_subview_parts();

    if(count($parts) < 2)
    {
        redirect("actions_list");
    }

    $identifier = $parts[0];
    $txid = $parts[1];

    $transaction = api('transaction', 'get', array("txid" => $txid));
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= $txid; ?>
            </div>
            <div class="panel-body">
                View inputs and outputs ...
                <?php
                    _print_r( getProperty($transaction, 'details', array())); 
                ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Summary
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <th>Action Identifier</th>
                        <td><?= getProperty($transaction, 'action identifier', 0); ?></td>
                    </tr>
                    <tr>
                        <th>Confirmations</th>
                        <td><?= getProperty($transaction, 'confirmations', 0); ?></td>
                    </tr>
                    <tr>
                        <th>Received Time</th>
                        <td><?= get_date($transaction, 'timereceived'); ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Summary
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <th>Total Input</th>
                        <td>0</td>
                    </tr>
                    <tr>
                        <th>Total Output</th>
                        <td>...</td>
                    </tr>
                    <tr>
                        <th>Fees</th>
                        <td>...</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Block
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <th>Block Index</th>
                        <td><?= getProperty($transaction, 'blockindex', 'Error'); ?></td>
                    </tr>
                    <tr>
                        <th>Block Time</th>
                        <td><?= get_date($transaction, 'blocktime'); ?></td>
                    </tr>
                    <tr>
                        <th>Block Hash</th>
                        <td>
                            <a href="<?= url_path('block/'.$identifier.'/'.getProperty($transaction, 'blockhash', '')); ?>">
                                <?= getProperty($transaction, 'blockhash', ''); ?>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>