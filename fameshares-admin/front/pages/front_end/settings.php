<?php
    
  
    $info =   $_SESSION["servers"][$_SESSION["actif_server"]]


    
?>
   

   <div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <svg class="glyph stroked external hard drive"><use xlink:href="#stroked-external-hard-drive"/></svg> Settings
           
        </div>
        <div class="panel-body">
           
            <form class="form-horizontal" action="<?= url_path('settings'); ?>" method="post">
                <div class="pull-right">
                <button type="submit" class="btn btn-primary" style="margin-left: 10px">Save</button>
                </div>
                <fieldset>
                     
                    <input name="id" hidden value="<?= $_SESSION["actif_server"] ?>"/>
                
                    <div class="form-group">
                         
                        <label class="col-md-3 control-label">Name</label>
                        <div class="col-md-7">
                            <?php input("name", "", $info, ''); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Server</label>
                        <div class="col-md-7">
                            <?php input("server", "http://", $info, 'http://127.0.0.1:1451'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">User</label>
                        <div class="col-md-7">
                            <?php input("username", "...", $info, 'NOTFOUND'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Pass</label>
                        <div class="col-md-7">
                             <?php input("password", "...", $info, 'NOTFOUND'); ?>
                        </div>
                    </div>
                
                </fieldset>
            </form>
            
         </div>
    </div>
</div>