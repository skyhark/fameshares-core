<?php
    $stats = api('wallet', 'stats');

    css("bootstrap-table.css");
    script("bootstrap-table.js");
?>
<style>
    
    .icon 
    {
        margin: 4px 0;
        font-size: 3em;
    }
    
    .extra_btns .btn
    {
        margin-bottom: 10px;
    }
    
</style>
<div class="row">
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-blue panel-widget ">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-usd icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= getProperty($stats, 'balance', 'Error'); ?></div>
                    <div class="text-muted">Balance</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-orange panel-widget">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-usd icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= getProperty($stats, 'unconfirmed_balance', 'Error'); ?></div>
                    <div class="text-muted">Unconfirmed Balance</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-teal panel-widget">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-usd icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= getProperty($stats, 'immature_balance', 'Error'); ?></div>
                    <div class="text-muted">Immature Balance</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-red panel-widget">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-info-sign icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= getProperty($stats, 'txcount', 'Error'); ?></div>
                    <div class="text-muted">Transactions Count</div>
                </div>
            </div>
        </div>
    </div>
</div><!--/.row-->



<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="tab-content">
                    <?php
                    
                        module('table', array(

                            "colls" => array("name", "balance", "address"),
                            "data" => api('wallet', 'accounts')

                        ));
                
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>