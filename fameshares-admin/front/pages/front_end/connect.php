   
   <?php
        $info = $_POST;
     
    ?>
   <div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <svg class="glyph stroked external hard drive"><use xlink:href="#stroked-external-hard-drive"/></svg> Connect
           
        </div>
        <div class="panel-body">
            <?php
                global $error;
                if(!is_null($error))
                {
            
            ?>
                <div class="alert alert-danger" role="alert">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <span class="sr-only">Error:</span>
                 <?= $error ?>
                </div>
            <?php
                    
                }
            ?>        
            <form class="form-horizontal" action="<?= url_path(''); ?>" method="post">
                <div class="pull-right">
                
                </div>
                <fieldset>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Name</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control server-name" name="name" value="<?= getProperty($info, 'name', ''); ?>"aria-label="...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Server</label>
                        <div class="col-md-7">
                               <div class="input-group">
                          <input type="text" name="server" class="form-control server-address"  value="<?= getProperty($info, 'server', ''); ?>" aria-label="...">
                      <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Servers <span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right ">
                           
                            
                              <input type="text" class="form-control search-input" placeholder="Search for...">
                        
                            
                          <li role="separator" class="divider"></li>
                          <?php
                                $var = api('servers', 'all');
                                foreach($var as $key => $server)
                                {
                                    $server = (array)$server;
                            ?>
                              <li><a class="ch-server" data-name="<?= $server["name"];?>" value="<?= $server["server"];?>"><?= $server["name"].' ' .$server["server"];?></a></li>
                          <?php 
                                }
                            ?>
                        </ul>
                      </div><!-- /btn-group -->
                    </div><!-- /input-group --> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">User</label>
                        <div class="col-md-7">
                            <?php input("username", "...", $info, ''); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Pass</label>
                        <div class="col-md-7">
                           <?php input("password", "...", $info, ''); ?>
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox col-md-3"></div>
                        <div class="checkbox col-md-3">
                            <label><input type="checkbox" name="new" value="">New server</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                    <button type="submit" class="btn btn-primary col-md-3" style="margin-left: 10px">Save</button>
                     </div>
                </fieldset>
            </form>
            
         </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(document).on('click' , '.ch-server', function(e)
        {
            $(".server-address").val($(this).attr("value"));
            $(".server-name").val($(this).attr("data-name"));
          
        });
        
        $(document).on('click' , '.search-input', function(e)
        {
          e.stopPropagation();
        });
        
        $(document).on('input' , '.search-input', function(e)
        {
            $s = $(".search-input").val().toLowerCase();
             
            if ($s === "")
            {
               $('.ch-server').show();
                 return;
            }
             $('.ch-server').hide(); 
             $(".ch-server").filter(function() {
                return $(this).text().toLowerCase().indexOf($s) >= 0;
            }).show();
        });       
                
});
</script>