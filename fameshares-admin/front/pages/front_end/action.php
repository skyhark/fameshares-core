<?php
    $parts = get_subview_parts();

    if(count($parts) < 1)
    {
        redirect("action_list");
    }

    $identifier = $parts[0];
    $stats = api('actions', 'stats', array("identifier" => $identifier));
    $lastblocks = getProperty($stats, 'last_blocks', array());
    $lasttrans = getProperty($stats, 'last_transactions', array());
?>
<h3 style="margin-top: 0"><?= $identifier; ?></h3>
<style>
    
    .icon 
    {
        margin: 4px 0;
        font-size: 3em;
    }
    
    .extra_btns .btn
    {
        margin-bottom: 10px;
    }
    
</style>
<div class="row">
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-blue panel-widget ">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-link icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= getProperty($stats, 'height', 0); ?></div>
                    <div class="text-muted">Chain Height</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-orange panel-widget">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-usd icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= number_format(getProperty($stats, 'balance', 0), 6); ?></div>
                    <div class="text-muted">Balance</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-12 col-lg-6">
        <div class="panel panel-teal panel-widget">
            <div class="row no-padding">
                <div class="col-sm-1 col-lg-2 widget-left">
                    <i class="glyphicon glyphicon-ban-circle icon"></i>
                </div>
                <div class="col-sm-11 col-lg-10 widget-right">
                    <div class="large"><?= date("Y-m-d H:i:s", getProperty($stats, 'creation_time', 0)); ?></div>
                    <div class="text-muted">Creation Date</div>
                </div>
            </div>
        </div>
    </div>
</div><!--/.row-->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Last Blocks
                
                <input type="text" class="form-control pull-right" placeholder="Search..." style="width: 200px" id="search">
            </div>
            <div class="panel-body">
                <table class="table table-hover">
                    <thead>
                        <th>Height</th>
                        <th>Creation Time</th>
                        <th>Transactions</th>
                        <th>Hash</th>
                    </thead>
                    <tbody>
                        <?php
                            foreach($lastblocks as $block)
                            {
                                if(empty($block))
                                {
                                    break;
                                }
                                $block = (array) $block;
                                print "<tr onclick='location.href=\"".url_path('block/'.$identifier.'/'.getProperty($block, 'hash', 'Error'))."\";'>";
                                print "<td>".getProperty($block, 'height', 'Error')."</td>";
                                print "<td>".get_date($block, 'time')."</td>";
                                print "<td>".count(getProperty($block, 'tx', array()))."</td>";
                                print "<td>".getProperty($block, 'hash', 'Error')."</td>";
                                print "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<hr>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Last Transactions
                
                <input type="text" class="form-control pull-right" placeholder="Search..." style="width: 200px" id="search_trans">
            </div>
            <div class="panel-body">
                <table class="table table-hover">
                    <thead>
                        <th>Tx ID</th>
                        <th>Address</th>
                        <th>Time</th>
                        <th>Amount</th>
                    </thead>
                    <tbody>
                        <?php
                            foreach($lasttrans as $trans)
                            {
                                if(empty($trans))
                                {
                                    break;
                                }
                                $trans = (array) $trans;
                                
                                print "<tr onclick='location.href=\"".url_path('transaction/'.$identifier.'/'.getProperty($trans, 'txid', 'Error'))."\";'>";
                                print "<td>".getProperty($trans, 'txid', 'Error')."</td>";
                                print "<td>".getProperty($trans, 'address', 'Error')."</td>";
                                print "<td>".get_date($trans, 'time')."</td>";
                                print "<td>".getProperty($trans, 'amount', 'Error')."</td>";
                                print "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<style>

    .table-hover tr td
    {
        cursor: pointer;
    }

</style>

<script>

    $("#search").keyup(function (e) {
        if (e.keyCode == 13) {
            location.href= "<?= url_path('block/'.$identifier.'/'); ?>"+$(this).val();
        }
    });
    
    $("#search_trans").keyup(function (e) {
        if (e.keyCode == 13) {
            location.href= "<?= url_path('transaction/'.$identifier.'/'); ?>"+$(this).val();
        }
    });
    
</script>