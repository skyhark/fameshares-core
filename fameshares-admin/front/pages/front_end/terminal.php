<?php
  global $types;
    $types = array("STRING" , "JSON" , "ARRAY");
    if(isset($_POST["command"]))
    {
        $res = null;
        $_POST['params'] = getProperty($_POST, 'params', array());
        
        if(isset($_POST['id']) && isset($_POST['name']))
        {
            $res = api('terminal', 'save');
        }
        else
        {
            $res = auto_cli($_POST["command"], $_POST['params']);
        }
        
        print json_encode($res);
        exit;
    }

    css("jjsonviewer.css");
    lib("jjsonviewer.js");
    lib("pages/terminal.js");

    $id = getNumericProperty(get_subview_parts(), 0, -1);
    $info = array();

    if($id != -1)
    {
        $info = (array) getProperty( api('terminal', 'read', array("id" => $id)), 0, $info );
    }
?>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <svg class="glyph stroked external hard drive"><use xlink:href="#stroked-external-hard-drive"/></svg> CLI Command
            <div class="pull-right">
                <button type="button" class="btn btn-default" onclick="open_action()">Open</button>
                <button type="button" class="btn btn-primary" style="margin-left: 10px" onclick="save()">Save</button>
            </div>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" action="<?= url_path('terminal'); ?>" method="post" id="frmterm">
                <fieldset>
                    <!-- Name input-->
                    <input type="hidden" name="id" value="<?= getProperty($info, 'id', -1); ?>">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Action Name</label>
                        <div class="col-md-7">
                            <?php input("name", "only on save", $info); ?>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Command</label>
                        <div class="col-md-7">
                            <?php input("command", "...", $info); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Additional</label>
                        <div class="col-md-7 funkyradio">
                            <div class="funkyradio-primary">
                                <input type="checkbox" name="review" id="review" <?= getProperty($info, 'review', false) ? 'checked' : ''; ?>/>
                                <label for="review" style="margin-top: 3px">A revoir</label>
                            </div>
                            <br>
                            
                            <textarea class="form-control" id="comment"><?= getProperty($info, 'comment', ''); ?></textarea>
                        </div>
                    </div>
                    <hr>
                    
                    <?php
                        $params = (array) json_decode( getProperty($info, 'params', "[]") );
                    ?>
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label">Parameters</label>
                        <div class="col-md-7">
                            <input name="params[]" type="text" placeholder="..."  value="<?= getProperty($params, 0, ''); ?>" class="form-control" />
                        </div>
                        <div class="col-md-2">
                        
                         <?php select(); ?> 
                       
                        </div>
                    </div>

                    <?php
                        function select( $selected="STRING" )
                        {
                            global $types;
                          echo '<select class="btn btn-primary chg-datatype" name="types[]">';
                            foreach($types as $val)
                            {
                              echo '<option value="'.$val.'" '.(($selected === $val)? 'selected="selected"': '') .' >'.$val.'</option>';
                            }
                         echo '</select>';
                        }
                            
                        
                        if(count($params) > 0)
                        {
                            array_shift($params);
                            foreach($params as $val)
                            {
                    ?>
                    <div class="form-group">
                        <div class="col-md-7 col-md-offset-3">
                            <input name="params[]" type="text" placeholder="..." class="form-control" value="<?= $val; ?>">
                        </div>
                         <div class="col-md-2">
                        
                          <?php select(); ?> 
                       
                        </div>
                    </div>
                    <?php
                            }
                    ?>
                    <div class="form-group">
                        <div class="col-md-7 col-md-offset-3">
                            <input name="params[]" type="text" placeholder="..." class="form-control">
                        </div>
                        <div class="col-md-2">
                        
                        <?php select(); ?> 
                        </div>
                    </div>
                    <?php
                        }
                    ?>

                    <!-- Form actions -->
                    <div class="form-group">
                        <div class="col-md-7 col-md-offset-3">
                            <input type="submit" class="btn btn-default btn-md" value="Submit">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <svg class="glyph stroked two-messages"><use xlink:href="#stroked-two-messages"></use></svg> Result 
            <time id="lstupd" style="color: red; margin-left: 20px"></time>
        </div>
        <div class="panel-body">
            <div id="jjson" class="jjson"></div>
        </div>
    </div>

</div>

<div class="modal fade" tabindex="-1" role="dialog" id="openmodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Open Action</h4>
      </div>
      <div class="modal-body">
            <div id="sidebar" class="sidebar-nav" style="max-height: 500px; overflow: auto">
                <?php
                    $actions = api('terminal', 'all');
                    $cat = null;
                    $others = array();
                
                    function action_li($action)
                    {
                        if(empty($action->name))
                        {
                            $action->name = "Action #".$action->id;
                        }
                        else 
                        {
                            $pos = strpos($action->name, '/');
                            if($pos !== false)
                            {
                                $action->name = substr($action->name, $pos+1);
                            }
                        }
                        
                        
                        print '<li><a href="'.url_path('terminal/').$action->id.'"';
                        
                        if($action->review == 1)
                        {
                            print ' style="color: red"';
                        }
                        
                        print '>'.$action->name.'</a></li>';
                    }
                
                    foreach($actions as $action)
                    {
                        $pos = strpos($action->name, '/');
                        
                        if($pos === false)
                        {
                            $others[] = $action;
                            continue;
                        }
                        
                        $ncat = substr($action->name, 0, $pos);
                        
                        if($ncat != $cat)
                        {
                            if($cat !== null)
                            {
                                print '</ul>';
                            }
                            
                            $cat = $ncat;
                            print '<h5><i class="glyphicon glyphicon-home"></i>';
                            print '<small><b> '.$ncat.'</b></small></h5>';
                            print '<ul class="nav nav-pills nav-stacked">';
                        }
                        
                        action_li($action);
                    }
                
                    if($cat !== null)
                    {
                        print '</ul>';
                    }
                ?>
                <h5><i class="glyphicon glyphicon-home"></i>
                    <small><b> Other Actions</b></small>
                </h5>
                <ul class="nav nav-pills nav-stacked">
                    <?php
                        foreach($others as $action)
                        {
                            action_li($action);
                        }
                    ?>
                </ul>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>
   
$(document).ready(function() { 
    
     var previous;
    function beauty($val){
        $temp="";
        try {
        $temp = JSON.parse($val);
        $temp=  JSON.stringify($val, undefined, 4);
        }
        catch(e)
        {
            $temp = $val;
        }
        return $temp;
        
    }
    $(document).on('focus', '.chg-datatype', function(data){
       previous = this.value;
        $(this).blur();
    });
    $(document).on('click', '.beauty',function(){ 
        $object = $(this).closest( ".form-group" ).find(".form-control");
        $object.val(beauty($object.val()))
        
    });
    
    $(document).on('change', '.chg-datatype',function(){ 
        var val = $(this).val();
        $parent = $(this).closest( ".form-group" );
        $object = $parent.find(".form-control");
        $attr = $object.attr("mutli_field_id");
        $val = $object.val();
        if($val === "")
            { 
            if(val === "JSON")
                { 
                $val ='{"menu": {\n'+
                      '"id": "file",\n'+
                      '"value": "File",\n'+
                      '"popup": {\n'+
                        '"menuitem": [\n'+
                         ' {"value": "New", "onclick": "CreateNewDoc()"},\n'+
                          '{"value": "Open", "onclick": "OpenDoc()"},\n'+
                          '{"value": "Close", "onclick": "CloseDoc()"}\n'+
                      '  ]\n'+
                      '}\n'+
                   ' }}\n';
                    $val = beauty($val);
                
                    
                }
            }
                    
                    
            

        if(previous === "STRING")
        {
            $object.replaceWith('<textarea rows="5" class="form-control"  multi_field_id="'+$attr+'"> '+$val+'</textarea>');
            $(this).after('<br> <a class="btn btn-success beauty btn-sm" >Beautify</a>')
        } 
        if(previous !== "STRING" && val === "STRING")
        {
            $parent.find(".beauty").replaceWith("");
            $object.replaceWith('<input name="params[]" type="text" placeholder="..."  value="'+$val+'" class="form-control" multi_field_id="'+$attr+'" />');
            
        } 
            
       
        
        console.log( previous + " --- " + val);
     
    });
});
                      
</script>

