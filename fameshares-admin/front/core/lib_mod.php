<?php

    function multi_path_find($path, $page)
    {
        if(file_exists( $path.$page.'.php' ))
        {
            return $path.$page.'.php';
        }
        else if(file_exists( $path.'front_end/'.$page.'.php' ))
        {
            return $path.'front_end/'.$page.'.php';
        }
        else if(file_exists( $path.'back_end/'.$page.'.php' ))
        {
            return $path.'back_end/'.$page.'.php';
        }
        else if(file_exists( $path.'admin/'.$page.'.php' ))
        {
            return $path.'admin/'.$page.'.php';
        }

        return null;
    }

    function module_path($modname)
    {
        $path = dirname(__FILE__)."/../modules/";
        return multi_path_find($path, $page);
    }

	function module($modName, $_settings=array())
	{
        $path = multi_path_find( dirname(__FILE__)."/../modules/", $modName);
        
        if($path == null)
        {
            return;
        }
        
		$_settings = (array) $_settings;
		$container = isset($_settings["container"]) ? $_settings["container"] : true;
		
		global $settings;
        $oldSettings = $settings;
        $settings = $_settings;
        
        include($path);

        global $settings;
        $settings = $oldSettings;
	}
	
	function library($libName)
	{
        $path = multi_path_find( dirname(__FILE__)."/../libraries/", $libName);
		include_once($path);
	}
	
?>