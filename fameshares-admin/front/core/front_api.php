<?php

    function front_api_handle($category, $action, $vars, $default = null)
    {
        foreach($vars as $var)
        {
            if(!isset($_POST[$var]))
            {
                /*if($default != null)
                {
                    return $default;
                }*/
                
                return false;
            }
        }
        
        $result = api($category, $action);
        
        if(is_view_only())
        {
            print json_encode($result);
            exit;
        }
        
        if($default != null)
        {
            $r = (array) $result;
            if(isset($r['error']))
            {
                return "<font color='red'>".$r['error']."</font>";
            }
            
            return $default;
        }
        
        return $result;
    }

?>