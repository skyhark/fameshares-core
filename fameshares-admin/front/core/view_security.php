<?php

    function get_view_config($base)
    {
        if(!file_exists($base.'config.json'))
        {
            return false;
        }
        
        $content = file_get_contents($base.'config.json');
        $config = (array) json_decode($content);

        if(!$config)
        {
            return false;
        }

        return $config;
    }

    function view_path_security_check($base, $page)
    {
        $config = get_view_config($base);
        global $view_config;
        $view_config = $config;
        
        if($config == false)
        {
            return $page;
        }

        if(getProperty($config, 'authentified', false) == false)
        {
            return $page;
        }
        
        if(!connected())
        {
            return '404';
        }
        
        
        //Check account type------
        
        
        
        
        //------------------------
        
        return $page;
    }

?>