<?php

function currency($number, $symbol="$")
{
    if(!is_numeric($number))
    {
        return $symbol." ". $number;
    }
	return $symbol." ". number_format($number, 2, ',', ' ');
}


function percentage($number)
{
    return number_format($number, 2, ',', ' ')."%";
}

?>