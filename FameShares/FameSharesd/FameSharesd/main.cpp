//
//  main.cpp
//  FameSharesd
//
//  Created by Sacha Vandamme on 23/03/16.
//  Copyright © 2016 Sacha Vandamme. All rights reserved.
//

#include <bitcoin_main.h>

int main(int argc, char* argv[])
{
    return main_daemon(argc, argv);
}
