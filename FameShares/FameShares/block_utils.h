//
//  block_utils.hpp
//  Lib-FameCoin
//
//  Created by Sacha Vandamme on 10/03/16.
//
//

#ifndef block_utils_hpp
#define block_utils_hpp

#include "primitives/block.h"
#include "fameshares.h"
#include "fchainparams.h"
#include <boost/filesystem.hpp>

enum FlushStateMode {
    FLUSH_STATE_NONE,
    FLUSH_STATE_IF_NEEDED,
    FLUSH_STATE_PERIODIC,
    FLUSH_STATE_ALWAYS
};

void CheckBlockIndex(std::string action_identifier, const Consensus::Params& consensusParams);
bool FlushStateToDisk(FAction *action, CValidationState &state, FlushStateMode mode);
bool FlushStateToDisk(FAction *action);
bool AcceptBlockHeader(const CBlockHeader& block, CValidationState& state, FChainParams *chainparams, CBlockIndex** ppindex=NULL);
bool ProcessNewBlock(CValidationState& state, FChainParams *chainparams, const CNode* pfrom, const CBlock* pblock, bool fForceProcessing, CDiskBlockPos* dbp);

class CBlockUndo;
extern int64_t nTimeBestReceived;
extern CConditionVariable cvBlockChange;

/** Functions for disk access for blocks */
bool WriteBlockToDisk(const CBlock& block, CDiskBlockPos& pos, const CMessageHeader::MessageStartChars& messageStart);
bool ReadBlockFromDisk(CBlock& block, const CDiskBlockPos& pos, const Consensus::Params& consensusParams);
bool ReadBlockFromDisk(CBlock& block, const CBlockIndex* pindex, const Consensus::Params& consensusParams);

bool UndoReadFromDisk(std::string action_identifier, CBlockUndo& blockundo, const CDiskBlockPos& pos, const uint256& hashBlock);

void UnloadBlockIndex(FAction *action);
bool LoadBlockIndex(FAction *action);
bool LoadExternalBlockFile(FChainParams *chainparams, FILE* fileIn, CDiskBlockPos *dbp = NULL);
boost::filesystem::path GetBlockPosFilename(std::string action_identifier, const CDiskBlockPos &pos, const char *prefix);
bool InitBlockIndex(FChainParams *chainparams);
void ThreadScriptCheck();

/** Functions for validating blocks and updating the block tree */

/** Context-independent validity checks */
bool CheckBlockHeader(const CBlockHeader& block, CValidationState& state, bool fCheckPOW = true);
bool CheckBlock(const CBlock& block, CValidationState& state, bool fCheckPOW = true, bool fCheckMerkleRoot = true);

/** Context-dependent validity checks.
 *  By "context", we mean only the previous block headers, but not the UTXO
 *  set; UTXO-related validity checks are done in ConnectBlock(). */
bool ContextualCheckBlockHeader(const CBlockHeader& block, CValidationState& state, CBlockIndex *pindexPrev);
bool ContextualCheckBlock(const CBlock& block, CValidationState& state, CBlockIndex *pindexPrev);

/** Apply the effects of this block (with given index) on the UTXO set represented by coins.
 *  Validity checks that depend on the UTXO set are also done; ConnectBlock()
 *  can fail if those validity checks fail (among other reasons). */
bool ConnectBlock(const CBlock& block, CValidationState& state, CBlockIndex* pindex, CCoinsViewCache& coins, bool fJustCheck = false);

/** Undo the effects of this block (with given index) on the UTXO set represented by coins.
 *  In case pfClean is provided, operation will try to be tolerant about errors, and *pfClean
 *  will be true if no problems were found. Otherwise, the return value will be false in case
 *  of problems. Note that in any case, coins may be modified. */
bool DisconnectBlock(const CBlock& block, CValidationState& state, const CBlockIndex* pindex, CCoinsViewCache& coins, bool* pfClean = NULL);

/** Check a block is completely valid from start to finish (only works on top of our current best block, with cs_main held) */
bool TestBlockValidity(CValidationState& state, FChainParams *chainparams, const CBlock& block, CBlockIndex* pindexPrev, bool fCheckPOW = true, bool fCheckMerkleRoot = true);

/** Find the last common block between the parameter chain and a locator. */
CBlockIndex* FindForkInGlobalIndex(const CChain& chain, const CBlockLocator& locator);

/** Mark a block as invalid. */
bool InvalidateBlock(CValidationState& state, const Consensus::Params& consensusParams, CBlockIndex *pindex);

/** Remove invalidity status from a block and its descendants. */
bool ReconsiderBlock(CValidationState& state, CBlockIndex *pindex);

bool ActivateBestChain(CValidationState &state, FChainParams *chainparams, const CBlock *pblock = NULL);
CAmount GetBlockSubsidy(int nHeight, const Consensus::Params& consensusParams);
/**
 * Return the spend height, which is one more than the inputs.GetBestBlock().
 * While checking, GetBestBlock() refers to the parent block. (protected by cs_main)
 * This is also true for mempool checks.
 */
int GetSpendHeight(const CCoinsViewCache& inputs);

bool IsInitialBlockDownload(std::string action_identifier);

void Misbehaving(NodeId nodeid, int howmuch);
void PruneOneBlockFile(std::string action_identifier, const int fileNumber);


void PruneAndFlush(FAction *action);

void PartitionCheck(bool (*initialDownloadCheck)(std::string), std::string action_identifier, CCriticalSection& cs, const CBlockIndex *const &bestHeader, int64_t nPowTargetSpacing);

#endif /* block_utils_hpp */
