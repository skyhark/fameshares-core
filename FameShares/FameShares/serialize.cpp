//
//  serialize.cpp
//  FameShares
//
//  Created by Sacha Vandamme on 27/03/16.
//  Copyright © 2016 Sacha Vandamme. All rights reserved.
//

#include "serialize.h"

#include "hash.h"

FWriter::FWriter(int nTypeIn, int nVersionIn)
: nType(nTypeIn), nVersion(nVersionIn), buffer_size(0)
{
    buffer = (char*) malloc(sizeof(char) * 1);
}

FWriter::FWriter()
    : nType(SER_GETHASH), nVersion(PROTOCOL_VERSION), buffer_size(0)
{
    buffer = (char*) malloc(sizeof(char) * 1);
}

FWriter::~FWriter()
{
    if(buffer != NULL)
    {
        free(buffer);
    }
    buffer = NULL;
}

FWriter& FWriter::write(char *pch, size_t size)
{
    buffer = (char*)realloc((void*) buffer, sizeof(char) * (buffer_size + size + 2));
    memmove(buffer + buffer_size, pch, size);
    buffer_size += size;
    
    /* for(int i = 0; i < (int) size; i++)
     {
     buffer[old_size + i] = pch[i];
     }*/
    
    return (*this);
}

char *FWriter::read()
{
    buffer[buffer_size] = 0;
    return buffer;
}

unsigned int FWriter::length()
{
    return buffer_size;
}
