#include "fameshares.h"
//#include <QStandardPaths>
//#include <QDir>
#include <iostream>
#include <boost/assign/list_of.hpp>
#include <stdexcept>
#include <stdio.h>

#include "chain.h"
#include "primitives/block.h"
#include "amount.h"
#include "coins.h"
#include "util.h"
#include "net_utils.h"
#include "init.h"
#include "ui_interface.h"
#include "timedata.h"
#include "utiltime.h"

#ifdef WIN32
boost::filesystem::path GetSpecialFolderPath(int nFolder, bool fCreate)
{
    namespace fs = boost::filesystem;
    
    char pszPath[MAX_PATH] = "";
    
    if(SHGetSpecialFolderPathA(NULL, pszPath, nFolder, fCreate))
    {
        return fs::path(pszPath);
    }
    
    LogPrintf("SHGetSpecialFolderPathA() failed, could not obtain requested path.\n");
    return fs::path("");
}
#endif

boost::filesystem::path GetDefaultDataDir()
{
    namespace fs = boost::filesystem;
    // Windows < Vista: C:\Documents and Settings\Username\Application Data\Bitcoin
    // Windows >= Vista: C:\Users\Username\AppData\Roaming\Bitcoin
    // Mac: ~/Library/Application Support/Bitcoin
    // Unix: ~/.bitcoin
#ifdef WIN32
    // Windows
    return GetSpecialFolderPath(CSIDL_APPDATA) / "FameShares";
#else
    fs::path pathRet;
    char* pszHome = getenv("HOME");
    if (pszHome == NULL || strlen(pszHome) == 0)
        pathRet = fs::path("/");
    else
        pathRet = fs::path(pszHome);
#ifdef MAC_OSX
    // Mac
    pathRet /= "Library/Application Support";
    TryCreateDirectory(pathRet);
    return pathRet / "FameShares";
#else
    // Unix
    return pathRet / ".FameShares";
#endif
#endif
}

FameShares::FameShares(AppMode _mode) :
    a_mode(_mode)
{
    //Database folder locating...
    namespace fs = boost::filesystem;

    if (mapArgs.count("-datadir"))
    {
        fs::path path = fs::system_complete(mapArgs["-datadir"]);
        if (fs::is_directory(path))
        {
            _root_path = path.string() + "/";
        }
        else
        {
            _root_path = GetDefaultDataDir().string() + "/";
        }
    }
    else
    {
        _root_path = GetDefaultDataDir().string() + "/";
    }
    
    fs::create_directories(_root_path);
    _config_path = _root_path + "fameshares.conf";

    if(_mode != mode_main)
    {
        _root_path = _root_path + mode_string() + "/";
        fs::create_directories(_root_path);
    } 

    _debug_path = _root_path + "debug.log";
    _pid_path = _root_path + "daemon.pid";
    _peers_path = _root_path + "peers.dat";

    //Create action directories..

    switch(_mode)
    {
    case mode_main:
        init_main_mode();
        break;
    case mode_regtest:
        init_regtest_mode();
        break;
    case mode_nettest:
        init_nettest_mode();
        break;
    }
}

std::string FameShares::action_path(std::string identifier)
{
    boost::filesystem::path dir = _root_path + identifier;
    
    if (!boost::filesystem::exists(dir))
    {
        boost::filesystem::create_directories(dir);
    }
    
    /*QDir dir(path.c_str());
    if (!dir.exists()) {
        dir.mkpath(".");
        //dir.mkpath("./blocks");
    }*/

    return dir.c_str();
}

std::string FameShares::debug_path()
{
    return _debug_path;
}

std::string FameShares::pid_path()
{
    return _pid_path;
}

std::string FameShares::config_path()
{
    return _config_path;
}

std::string FameShares::path(std::string custom)
{
    return _root_path + custom;
}

std::string FameShares::peers_path()
{
    return _peers_path;
}

std::string FameShares::action_state_path(std::string identifier)
{
    return action_path(identifier) + "/chainstate";
}

std::string FameShares::action_index_path(std::string identifier)
{
    return action_path(identifier) + "/blocks/index";
}

UniValue FameShares::read_json(std::string path)
{
    FILE *f = fopen(path.c_str(), "r");
    std::string jdata;
    
    char buf[4096];
    while (!feof(f)) {
        int bread = fread(buf, 1, sizeof(buf), f);
        assert(!ferror(f));
        
        std::string s(buf, bread);
        jdata += s;
    }
    
    assert(!ferror(f));
    fclose(f);

    UniValue result;
    result.read(jdata);
    return result;
}

void mining_test(FAction *action)
{
    CBlock block = action->params->GenesisBlock();

    std::cout << block.GetCachedHash().ToString() << "\n" << block.GetHash().ToString() << "\n";
    
    //assert(ss1.GetHash().ToString().compare( block.GetHash().ToString() ));
    
    
    bool fNegative;
    bool fOverflow;
    arith_uint256 bnTarget;
    bnTarget.SetCompact(block.nBits, &fNegative, &fOverflow);
    
    //int64_t time = GetTimeMillis();
    block.nNonce = 0;
    
    int64_t s, ser_time = 0, hash_time = 0;
    
    while (block.nNonce < 1000000)
    {
        s = GetTimeMillis();
        block.GetCachedHash();
        ser_time += (GetTimeMillis() - s);
        
        s = GetTimeMillis();
        block.GetHash();
        hash_time += (GetTimeMillis() - s);

        
        
        //block.GetHash();
        
        /*if(UintToArith256(block.GetHash()) <= bnTarget)
        {
            break;
        }*/
        
        ++block.nNonce;
    }
    
   // time = GetTimeMillis() - time;
    std::cout << "Serialize Time: " << ser_time << ", Hash time: " << hash_time <<  "\n";
    exit (EXIT_FAILURE);

}

void FameShares::regenerate_genesis(CBlock block, uint32_t start, uint32_t end)
{
    std::cout << "Searching for genesis block (" << block.action_identifier << ")...\n";
    
    FChainParams *params = action(block.action_identifier)->params;
    block.nNonce = start;
    
    bool fNegative;
    bool fOverflow;
    arith_uint256 bnTarget;
    bnTarget.SetCompact(block.nBits, &fNegative, &fOverflow);

    std::cout << "Initial bnTarget: " << bnTarget.ToString() << " (" << block.nBits << ")\n";
    std::cout << "Initial Limit: " << UintToArith256(params->GetConsensus().powLimit).ToString() << "\n";

    // Check range
    if (fNegative || bnTarget == 0 || fOverflow || bnTarget > UintToArith256(params->GetConsensus().powLimit))
    {
        std::cout << "-----------Nonce not found-----------\n";
        std::cout << "BITS OUT OF RANGE ! \n";
        
        if(fNegative)
        {
            std::cout << "Bits value too big (fNegative: " << (fNegative ? "true" : "false") << ")\n";
        }
        else if(bnTarget == 0)
        {
            std::cout << "Bits value too small (bnTarget is 0)\n";
        }
        else if(fOverflow || bnTarget > UintToArith256(params->GetConsensus().powLimit))
        {
            std::cout << "Bits value too big (fOverflow: " << (fOverflow ? "true" : "false") << ")\\n";
        }
        
        return;
    }

    arith_uint256 best_hash = UintToArith256(block.GetCachedHash());
    arith_uint256 nw = UintToArith256(block.GetCachedHash());
    
    while (nw > bnTarget && block.nNonce < end) {
        ++block.nNonce;
        nw = UintToArith256(block.GetCachedHash());

        if(nw < best_hash)
        {
            best_hash = nw;
        }
        
        if(block.nNonce % 10000000 == 0)
        {
            std::cout << block.nNonce << "\n";
        }
    }

    
    if(CheckProofOfWork(block.GetHash(),block.nBits, params->GetConsensus()))
    {
        std::cout << "-----------NONCE FOUND-----------\n";
        printf("block.nTime = %u \n", block.nTime);
        printf("block.nNonce = %u \n", block.nNonce);
        printf("block.GetHash = %s\n", block.GetHash().ToString().c_str());
    }
    else
    {
        std::cout << "-----------Nonce not found-----------\n";
        std::cout << "Best Hash found: " << best_hash.ToString().c_str() << "\n";
    }

}

void FameShares::import_data(std::string identifier, UniValue val)
{
    if(identifier.empty())
    {
        return;
    }
    
    UniValue ConsParams = val["Consensus"];
    UniValue GenParams = val["Genesis"];
    
    bool reconcider = (identifier == GetArg("-findgenesis", ""));
    
    //Generate consensus
    Consensus::Params consensus;
    consensus.nSubsidyHalvingInterval = ConsParams["HalvingInterval"].get_int();
    consensus.nMajorityEnforceBlockUpgrade = ConsParams["MajorityEnforceBlockUpgrade"].get_int();
    consensus.nMajorityRejectBlockOutdated = ConsParams["MajorityRejectBlockOutdated"].get_int();
    consensus.nMajorityWindow = ConsParams["MajorityWindow"].get_int();
    consensus.BIP34Height = ConsParams["BIP34Height"].get_int();
    consensus.BIP34Hash = uint256S(ConsParams["BIP34Hash"].get_str());
    consensus.powLimit = uint256S(ConsParams["powLimit"].get_str());
    consensus.nPowTargetTimespan = ConsParams["PowTargetTimespan"].get_int(); // two weeks
    consensus.nPowTargetSpacing = ConsParams["PowTargetSpacing"].get_int();
    consensus.fPowAllowMinDifficultyBlocks = ConsParams["PowAllowMinDifficultyBlocks"].get_bool();
    consensus.fPowNoRetargeting = ConsParams["PowNoRetargeting"].get_bool();

    //Generate genesis
    CBlock genesis = CreateGenesisBlock(identifier,
                            GenParams["Time"].get_int64(),
                            GenParams["Nonce"].get_int64(),
                            GenParams["Bits"].get_int64(),
                            GenParams["Version"].get_int(),
                            GenParams["Amount"].get_int() * COIN);

    consensus.hashGenesisBlock = genesis.GetHash();
    
    if(!reconcider)
    {
        if(consensus.hashGenesisBlock != uint256S(ConsParams["hashGenesisBlock"].get_str()))
        {
            std::cout << "Expected hashGenesisBlock for " << identifier << ": " << consensus.hashGenesisBlock.ToString()  << "\n";
        }
        
        assert(consensus.hashGenesisBlock == uint256S(ConsParams["hashGenesisBlock"].get_str()));
    }
    
    if(genesis.hashMerkleRoot != uint256S(GenParams["HashMerkleRoot"].get_str()))
    {
        std::cout << "Expected hashMerkleRoot for " << identifier << ": " << genesis.hashMerkleRoot.ToString()  << "\n";
    }
    
    assert(genesis.hashMerkleRoot == uint256S(GenParams["HashMerkleRoot"].get_str()));
    
    //Generate checkpoints
    CCheckpointData checkpointData;
    
    FChainParams *params = new FChainParams(genesis, consensus, checkpointData);
    /*FAction *action =*/ add_action(params);
    
    //mining_test(action);

    if(reconcider)
    {
         regenerate_genesis(genesis, GetArg("-start", 0), GetArg("-end", 4294967294LL));
         exit (EXIT_FAILURE);
    }
}

void FameShares::init_actions()
{
    UniValue conf = read_json( path("actions.json") );

    BOOST_FOREACH( std::string key, conf.getKeys() )
    {
        UniValue val = conf[key];
        import_data(key, val);
    }
    
}

std::string FameShares::mode_string(bool empty_main)
{
    switch(a_mode)
    {
    case mode_main:
        return empty_main ? "" : "main";
        break;
    case mode_nettest:
        return "nettest";
        break;
    case mode_regtest:
        return "regtest";
        break;
    }

    return "";
}

AppMode FameShares::mode()
{
    return a_mode;
}

FAction *FameShares::action(std::string identifier)
{
    if (!actions.count(identifier))
    {
        /*FAction *act = new FAction(identifier);
        actions[identifier] = act;
        act->db = new CBlockTreeDB(identifier, false, GetBoolArg("-reindex", false));
        return act;*/
        std::cout << "Action not found: " << identifier << "\n";
        LogPrintStr("Action not found: " + identifier + "\n");
        throw std::runtime_error("Action " + identifier + " not found");
    }

    return actions[identifier];
}

FAction *FameShares::add_action(FChainParams *params)
{
    std::string action_identifier = params->action_identifier();
    ActionsMap::const_iterator got = actions.find(action_identifier);

    if (got != actions.end())
    {
        delete actions[action_identifier];
    }

    FAction *act = new FAction(action_identifier);
    actions[action_identifier] = act;

    act->params = params;
    //act->db = new CBlockTreeDB(action_identifier, false, GetBoolArg("-reindex", false));
    
    return act;
}

CBlockTreeDB *FameShares::db(std::string identifier)
{
    return action(identifier)->db;
}

CChain *FameShares::chain(std::string identifier)
{
    return action(identifier)->chain;
}

CCoinsViewCache *FameShares::coins(std::string identifier)
{
    return action(identifier)->coins;
}

BlockMap FameShares::blocks(std::string identifier)
{
    return action(identifier)->blocks;
}

FChainParams *FameShares::params(std::string identifier)
{
    return action(identifier)->params;
}

unsigned short FameShares::default_port(AppMode _mode)
{
    return (_mode == mode_main ? 1450 : 1950);
}

unsigned short FameShares::default_RPCPort(AppMode _mode)
{
    return (_mode == mode_main ? 1451 : 1951);
}

unsigned short FameShares::port()
{
    return (unsigned short)(GetArg("-port", default_port(mode())));
}

unsigned short FameShares::RPCPort()
{
    return (unsigned short)(GetArg("-rpcport", default_RPCPort(mode())));
}

bool FameShares::DefaultConsistencyChecks()
{
    return (mode() == mode_regtest);
}

ActionsMap FameShares::get_actions()
{
    return actions;
}

/*Init mode*/

void FameShares::init_main_mode()
{
    base58Prefixes[PUBKEY_ADDRESS] = std::vector<unsigned char>(1,0);
    base58Prefixes[SCRIPT_ADDRESS] = std::vector<unsigned char>(1,5);
    base58Prefixes[SECRET_KEY]     = std::vector<unsigned char>(1,128);
    base58Prefixes[EXT_PUBLIC_KEY] = boost::assign::list_of(0x04)(0x88)(0xB2)(0x1E).convert_to_container<std::vector<unsigned char> >();
    base58Prefixes[EXT_SECRET_KEY] = boost::assign::list_of(0x04)(0x88)(0xAD)(0xE4).convert_to_container<std::vector<unsigned char> >();

    pchMessageStart[0] = 0xa1;
    pchMessageStart[1] = 0xdf;
    pchMessageStart[2] = 0x08;
    pchMessageStart[3] = 0x6f;

    vSeeds.push_back(CDNSSeedData("famebroker.com", "seeds.famebroker.com"));
    nPruneAfterHeight = 100000;
    fMineBlocksOnDemand = true;
    fMiningRequiresPeers = false;
    fDefaultConsistencyChecks = false;
    fRequireStandard = true;

    vAlertPubKey = ParseHex("04fc9702847840bbf195de8442ebecedf5b095cdbb9bc716bda9110971b28a49e0ead8564ff0db22209e0374782c093bb899692d524e9d6a6956e7c5ecbcd68284");
}

void FameShares::init_regtest_mode()
{
    nPruneAfterHeight = 100000;
    fMiningRequiresPeers = false;
    fDefaultConsistencyChecks = true;
    fRequireStandard = false;
}

void FameShares::init_nettest_mode()
{
    base58Prefixes[PUBKEY_ADDRESS] = std::vector<unsigned char>(1,111);
    base58Prefixes[SCRIPT_ADDRESS] = std::vector<unsigned char>(1,196);
    base58Prefixes[SECRET_KEY] =     std::vector<unsigned char>(1,239);
    base58Prefixes[EXT_PUBLIC_KEY] = boost::assign::list_of(0x04)(0x35)(0x87)(0xCF).convert_to_container<std::vector<unsigned char> >();
    base58Prefixes[EXT_SECRET_KEY] = boost::assign::list_of(0x04)(0x35)(0x83)(0x94).convert_to_container<std::vector<unsigned char> >();

    pchMessageStart[0] = 0xc2;
    pchMessageStart[1] = 0xff;
    pchMessageStart[2] = 0xa3;
    pchMessageStart[3] = 0x9b;

    nPruneAfterHeight = 100000;
    fMineBlocksOnDemand = false;
    fMiningRequiresPeers = true;
    fDefaultConsistencyChecks = false;
    fRequireStandard = false;
    
    vAlertPubKey = ParseHex("04302390343f91cc457d56d68b123028bf52e5fca1939df127f63c6467cdf9c8e2c14b61104cf817d0b780da337893ecc4aaff1309e536162dabbdb45200ca2b0a");
}

/*------------------------------------------------------*/

static FameShares *main_shares = 0;
#include "util.h"
void init_shares(AppMode mode)
{
    main_shares = new FameShares(mode);

    fPrintToConsole = GetBoolArg("-printtoconsole", false);
    fLogTimestamps = GetBoolArg("-logtimestamps", DEFAULT_LOGTIMESTAMPS);
    fLogTimeMicros = GetBoolArg("-logtimemicros", DEFAULT_LOGTIMEMICROS);
    fLogIPs = GetBoolArg("-logips", DEFAULT_LOGIPS);
    
    main_shares->init_actions();
/*
    if(!fPrintToConsole)
    {
        OpenDebugLog();
    }

    LogPrintf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    LogPrintf("Bitcoin version %s (%s)\n", FormatFullVersion(), CLIENT_DATE);
*/
}

void close_shares()
{
    if(main_shares != 0)
    {
        delete main_shares;
    }
}

FameShares *shares()
{
    if(main_shares == 0)
    {
        //throw InitException;
    }

    return main_shares;
}

/*------------------------------------------------------*/

FAction::FAction(std::string action_identifier) :
    coins(0),
    chain(new CChain(action_identifier)),
    params(0),
    db(0),
    coinsdbview(0),
    coinscatcher(0),
    nLastBlockFile(0),
    fCheckForPruning(false),
    pindexBestHeader(NULL),
    pindexBestInvalid(NULL),
    nBlockSequenceId(1),
    nLastBlockTx(0),
    nLastBlockSize(0)
{
    fHavePruned = false;
    shares()->action_path(action_identifier);
    fReindex = GetBoolArg("-reindex", false);
    
    LogPrintf("Action %s initialised\n", action_identifier);
}

FAction::~FAction()
{
    shutdown();

    //Delete all blocks check
}

std::string FAction::action_identifier()
{
    return params->action_identifier();
}

void FAction::init_blocks()
{
    boost::filesystem::path blocksDir = shares()->action_path( action_identifier() ) + "/blocks";
    if (!boost::filesystem::exists(blocksDir))
    {
        boost::filesystem::create_directories(blocksDir);
        /*bool linked = false;
        for (unsigned int i = 1; i < 10000; i++) {
            boost::filesystem::path source = shares()->path( strprintf("blk%04u.dat", i) );
            if (!boost::filesystem::exists(source)) break;
            boost::filesystem::path dest = blocksDir / strprintf("blk%05u.dat", i-1);
            try {
                boost::filesystem::create_hard_link(source, dest);
                LogPrintf("Hardlinked %s -> %s\n", source.string(), dest.string());
                linked = true;
            } catch (const boost::filesystem::filesystem_error& e) {
                // Note: hardlink creation failing is not a disaster, it just means
                // blocks will get re-downloaded from peers.
                LogPrintf("Error hardlinking blk%04u.dat: %s\n", i, e.what());
                break;
            }
        }
        if (linked)
        {
            fReindex = true;
        }*/
    }
}

bool FAction::load_blocks(int64_t nBlockTreeDBCache, int64_t nCoinDBCache)
{
    bool fLoaded = false;
    while (!fLoaded) {
        bool fReset = fReindex;
        std::string strLoadError;
        
        uiInterface.InitMessage( strprintf("Loading block index for %s ...", action_identifier()) );
        
        do {
            try {
                UnloadBlockIndex(this);
                if(coins != 0) { delete coins; }
                if(db != 0) { delete db; }
                if(coinsdbview != 0) { delete coinsdbview; }
                if(coinscatcher != 0) { delete coinscatcher; }

                db = new CBlockTreeDB(action_identifier(), nBlockTreeDBCache, false, fReindex);
                coinsdbview = new CCoinsViewDB(action_identifier(), nCoinDBCache, false, fReindex);
                coinscatcher = new CCoinsViewErrorCatcher(coinsdbview);
                coins = new CCoinsViewCache(coinscatcher);
                
                if (fReindex) {
                    db->WriteReindexing(true);
                    //If we're reindexing in prune mode, wipe away unusable block files and all undo data files
                    if (fPruneMode)
                        CleanupBlockRevFiles( action_identifier() );
                }
                
                if (!LoadBlockIndex(this)) {
                    strLoadError = _("Error loading block database");
                    break;
                }
                
                // If the loaded chain has a wrong genesis, bail out immediately
                // (we're likely using a testnet datadir, or the other way around).
                if (!blocks.empty() && blocks.count(params->GetConsensus().hashGenesisBlock) == 0)
                    return InitError(_("Incorrect or no genesis block found. Wrong datadir for network?"));
                
                // Initialize the block index (no-op if non-empty database was already loaded)
                if (!InitBlockIndex(params)) {
                    strLoadError = _("Error initializing block database");
                    break;
                }
                
                // Check for changed -txindex state
                if (fTxIndex != GetBoolArg("-txindex", DEFAULT_TXINDEX)) {
                    strLoadError = _("You need to rebuild the database using -reindex to change -txindex");
                    break;
                }
                
                // Check for changed -prune state.  What we are concerned about is a user who has pruned blocks
                // in the past, but is now trying to run unpruned.
                if (fHavePruned && !fPruneMode) {
                    strLoadError = _("You need to rebuild the database using -reindex to go back to unpruned mode.  This will redownload the entire blockchain");
                    break;
                }
                
                uiInterface.InitMessage(_("Verifying blocks..."));
                if (fHavePruned && GetArg("-checkblocks", DEFAULT_CHECKBLOCKS) > MIN_BLOCKS_TO_KEEP) {
                    LogPrintf("Prune: pruned datadir may not have more than %d blocks; -checkblocks=%d may fail\n",
                              MIN_BLOCKS_TO_KEEP, GetArg("-checkblocks", DEFAULT_CHECKBLOCKS));
                }
                
                {
                    LOCK(cs_main);
                    CBlockIndex* tip = chain->Tip();
                    if (tip && tip->nTime > GetAdjustedTime() + 2 * 60 * 60) {
                        strLoadError = _("The block database contains a block which appears to be from the future. "
                                         "This may be due to your computer's date and time being set incorrectly. "
                                         "Only rebuild the block database if you are sure that your computer's date and time are correct");
                        break;
                    }
                }
                
                if (!CVerifyDB().VerifyDB(params, coinsdbview, GetArg("-checklevel", DEFAULT_CHECKLEVEL),
                                          GetArg("-checkblocks", DEFAULT_CHECKBLOCKS))) {
                    strLoadError = _("Corrupted block database detected");
                    break;
                }
            } catch (const std::exception& e) {
                if (fDebug) LogPrintf("%s\n", e.what());
                strLoadError = _("Error opening block database");
                break;
            }
            
            fLoaded = true;
        } while(false);
        
        if (!fLoaded) {
            // first suggest a reindex
            if (!fReset) {
                bool fRet = uiInterface.ThreadSafeMessageBox(
                                                             strLoadError + ".\n\n" + _("Do you want to rebuild the block database now?"),
                                                             "", CClientUIInterface::MSG_ERROR | CClientUIInterface::BTN_ABORT);
                if (fRet) {
                    fReindex = true;
                    //fRequestShutdown = false;
                } else {
                    LogPrintf("Aborted block database rebuild. Exiting.\n");
                    return false;
                }
            } else {
                return InitError(strLoadError);
            }
        }
    }

    return true;
}


void FAction::shutdown()
{
    LOCK(cs_main);
    if (coins != 0) {
        FlushStateToDisk(this);
    }
    
    if(coins != 0) { delete coins; coins = 0; }
    if(chain != 0) { delete chain; chain = 0; }
    if(params != 0) { delete params; params = 0; }
    if(db != 0) { delete db; db = 0; }
    if(coinsdbview != 0) { delete coinsdbview; coinsdbview = 0; }
    if(coinscatcher != 0) { delete coinscatcher; coinscatcher = 0; }
}


