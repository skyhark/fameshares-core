#ifndef NET_UTILS_H
#define NET_UTILS_H

#include "uint256.h"
#include <map>
#include "net.h"
#include "consensus/params.h"
#include "chain.h"
#include "primitives/block.h"
#include <string>
#include <boost/unordered_map.hpp>

extern bool fLargeWorkForkFound;
extern bool fLargeWorkInvalidChainFound;

class CBlockIndex;

struct CDNSSeedData {
    std::string name, host;
    CDNSSeedData(const std::string &strName, const std::string &strHost) : name(strName), host(strHost) {}
};

struct SeedSpec6 {
    uint8_t addr[16];
    uint16_t port;
};

typedef std::map<int, uint256> MapCheckpoints;

struct CCheckpointData {
    MapCheckpoints mapCheckpoints;
    int64_t nTimeLastCheckpoint;
    int64_t nTransactionsLastCheckpoint;
    double fTransactionsPerDay;
    std::string action_identifier;
};

struct CNodeStateStats {
    int nMisbehavior;
    int nSyncHeight;
    int nCommonHeight;
    std::vector<int> vHeightInFlight;
};

struct QueuedBlock {
    uint256 hash;
    CBlockIndex *pindex;  //! Optional.
    int64_t nTime;  //! Time of "getdata" request in microseconds.
    bool fValidatedHeaders;  //! Whether this block has validated headers at the time of request.
    int64_t nTimeDisconnect; //! The timeout for this block request (for disconnecting a slow peer)
    std::string action_identifier;
    
    //QueuedBlock(std::string _action_identifier) : action_identifier(_action_identifier) { }
};
struct CBlockReject {
    unsigned char chRejectCode;
    std::string strRejectReason;
    uint256 hashBlock;
    std::string action_identifier;
};
struct CNodeActionState
{
    std::string action_identifier;
    //! The best known block we know this peer has announced.
    CBlockIndex *pindexBestKnownBlock;
    //! The hash of the last unknown block this peer has announced.
    uint256 hashLastUnknownBlock;
    //! The last full block we both have.
    CBlockIndex *pindexLastCommonBlock;
    //! The best header we have sent our peer.
    CBlockIndex *pindexBestHeaderSent;
    
    CNodeActionState(std::string _action_identifier)
    {
        action_identifier = _action_identifier;
        pindexBestKnownBlock = NULL;
        hashLastUnknownBlock.SetNull();
        pindexLastCommonBlock = NULL;
        pindexBestHeaderSent = NULL;
    }
};
typedef boost::unordered_map<std::string, CNodeActionState*> NodeActionStateMap;

struct CNodeState {
    //! The peer's address
    CService address;
    //! Whether we have a fully established connection.
    bool fCurrentlyConnected;
    //! Accumulated misbehaviour score for this peer.
    int nMisbehavior;
    //! Whether this peer should be disconnected and banned (unless whitelisted).
    bool fShouldBan;
    //! String name of this peer (debugging/logging purposes).
    std::string name;
    //! List of asynchronously-determined block rejections to notify this peer about.
    std::vector<CBlockReject> rejects;
    
    NodeActionStateMap action_states;
    
    //! Whether we've started headers synchronization with this peer.
    bool fSyncStarted;
    //! Since when we're stalling block download progress (in microseconds), or 0.
    int64_t nStallingSince;
    std::list<QueuedBlock> vBlocksInFlight;
    int nBlocksInFlight;
    int nBlocksInFlightValidHeaders;
    //! Whether we consider this a preferred download peer.
    bool fPreferredDownload;
    //! Whether this peer wants invs or headers (when possible) for block announcements.
    bool fPreferHeaders;
    
    CNodeState();
    CNodeActionState *state(std::string action_identifier);
};


class CMainCleanup
{
public:
    CMainCleanup() {}
    ~CMainCleanup();
};

extern CMainCleanup instance_of_cmaincleanup;

//--------------------------------------------------------------------

CNodeState *State(NodeId pnode);

bool MarkBlockAsReceived(const uint256& hash);
/*bool static AlreadyHave(const CInv& inv);
void static ProcessGetData(CNode* pfrom, std::string action_identifier, const Consensus::Params& consensusParams);
bool static ProcessMessage(CNode* pfrom, std::string action_identifier, std::string strCommand, CDataStream& vRecv, int64_t nTimeReceived);*/
bool ProcessMessages(CNode* pfrom);
bool SendMessages(std::string action_identifier, CNode* pto);
bool GetNodeStateStats(std::string action_identifier ,NodeId nodeid, CNodeStateStats &stats);
std::string GetWarnings(const std::string& strFor);

void RegisterNodeSignals(CNodeSignals& nodeSignals);
void UnregisterNodeSignals(CNodeSignals& nodeSignals);
CBlockIndex* FindForkInGlobalIndex(const CChain& chain, const CBlockLocator& locator);

#endif // NET_UTILS_H
