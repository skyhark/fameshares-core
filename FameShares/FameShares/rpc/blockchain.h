#ifndef BITCOIN_BLOCKCHAINF_H
#define BITCOIN_BLOCKCHAINF_H

#include <univalue/univalue.h>
#include "bitcoin_main.h"

class CBlock;
class CBlockIndex;

EXPORT UniValue blockToJSON(const CBlock& block, const CBlockIndex* blockindex, bool txDetails = false);

#endif
