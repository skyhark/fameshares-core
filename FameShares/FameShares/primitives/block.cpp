// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2015 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "block.h"

#include "hash.h"
#include "tinyformat.h"
#include "utilstrencodings.h"
#include "../crypto/common.h"

uint256 CBlockHeader::GetHash() const
{
    //return SerializeHash(*this);
    
    CHashWriter ss(SER_GETHASH, PROTOCOL_VERSION);
    ss << *this;
    return ss.GetHash();

}

/*uint256 CBlockHeader::GetCachedHash() const
{
    CHashWriter ss(SER_GETHASH, PROTOCOL_VERSION);
    cached_header_serialze(ss, SER_GETHASH, PROTOCOL_VERSION);
    return ss.GetHash();
}*/

std::string CBlock::ToString() const
{
    std::stringstream s;
    s << strprintf("CBlock(hash=%s, ver=%d, hashPrevBlock=%s, hashMerkleRoot=%s, nTime=%u, nBits=%08x, nNonce=%u, action_identifier=%s, vtx=%u)\n",
        GetHash().ToString(),
        nVersion,
        hashPrevBlock.ToString(),
        hashMerkleRoot.ToString(),
        nTime, nBits, nNonce,
        action_identifier,
        vtx.size());
    for (unsigned int i = 0; i < vtx.size(); i++)
    {
        s << "  " << vtx[i].ToString() << "\n";
    }
    return s.str();
}
