//
//  blockfile.c
//  Lib-FameCoin
//
//  Created by Sacha Vandamme on 20/03/16.
//
//

#include "blockinfo.h"

#include <string.h>
#include "utils.h"
#include "utilstrencodings.h"
#include "timedata.h"

std::string CBlockFileInfo::ToString() const {
    return strprintf("CBlockFileInfo(blocks=%u, size=%u, heights=%u...%u, time=%s...%s)", nBlocks, nSize, nHeightFirst, nHeightLast, DateTimeStrFormat("%Y-%m-%d", nTimeFirst), DateTimeStrFormat("%Y-%m-%d", nTimeLast));
}
