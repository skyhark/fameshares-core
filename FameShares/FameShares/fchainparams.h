#ifndef FCHAINPARAMS_H
#define FCHAINPARAMS_H

#include "consensus/params.h"
#include "primitives/block.h"
#include "net_utils.h"
//#include "protocol.h"


typedef std::map<int, uint256> MapCheckpoints;

CBlock CreateGenesisBlock(std::string action_identifier, const char* pszTimestamp, const CScript& genesisOutputScript, uint32_t nTime, uint32_t nNonce, uint32_t nBits, int32_t nVersion, const CAmount& genesisReward);
CBlock CreateGenesisBlock(std::string action_identifier, uint32_t nTime, uint32_t nNonce, uint32_t nBits, int32_t nVersion, const CAmount& genesisReward);
/**
 * CChainParams defines various tweakable parameters of a given instance of the
 * Bitcoin system. There are three: the main network on which people trade goods
 * and services, the public test network which gets reset from time to time and
 * a regression test mode which is intended for private networks only. It has
 * minimal difficulty to ensure that blocks can be found instantly.
 */

class FChainParams
{
public:
    FChainParams(CBlock _genesis, Consensus::Params _consensus, CCheckpointData _checkpointData) :
        consensus(_consensus), genesis(_genesis), checkpointData(_checkpointData), _action_identifier(_genesis.action_identifier)
    {
        _checkpointData.action_identifier = _action_identifier;
    }

    const Consensus::Params& GetConsensus() const { return consensus; }
    const CBlock& GenesisBlock() const { return genesis; }

    const CCheckpointData& Checkpoints() const { return checkpointData; }
    std::string action_identifier() { return _action_identifier; }
protected:
    FChainParams(std::string paction_identifier) : _action_identifier(paction_identifier) {
        checkpointData.action_identifier = paction_identifier;
    }

    Consensus::Params consensus;
    CBlock genesis;
    CCheckpointData checkpointData;
    std::string _action_identifier;
};

#endif // FCHAINPARAMS_H
