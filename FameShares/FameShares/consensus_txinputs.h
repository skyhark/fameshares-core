#ifndef CONSENSUS_TXINPUTS_H
#define CONSENSUS_TXINPUTS_H

#include "utilmoneystr.h"
#include "utilstrencodings.h"
#include "primitives/transaction.h"
#include "validationinterface.h"
#include "db_utils.h"

namespace Consensus {
    bool CheckTxInputs(const CTransaction& tx, CValidationState& state, const CCoinsViewCache& inputs, int nSpendHeight);
}// namespace Consensus

#endif // CONSENSUS_TXINPUTS_H
