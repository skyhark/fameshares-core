#ifndef DB_UTILS_H
#define DB_UTILS_H

#if defined(HAVE_CONFIG_H)
#include "config/bitcoin-config.h"
#endif

#include "amount.h"
#include "chain.h"
#include "coins.h"
#include "net.h"
#include "script/script_error.h"
#include "sync.h"
#include "consensus/merkle.h"
#include "trans_utils.h"
#include "utilstrencodings.h"
#include "fameshares.h"

#include <algorithm>
#include <exception>
#include <map>
#include <set>
#include <stdint.h>
#include <string>
#include <utility>
#include <vector>

#include <boost/unordered_map.hpp>
#include "cscriptcheck.h"

class FameShares;
struct FAction;
class CTxMemPool;
class CValidationState;
class CBlockUndo;
class CCoinsViewCache;
class FChainParams;
class CBlockIndex;
struct BlockHasher;

extern int nPreferredDownload;
extern int nSyncStarted;
extern int nQueuedValidatedHeaders;

extern std::map<NodeId, CNodeState> mapNodeState;
extern std::map<uint256, std::pair<NodeId, std::list<QueuedBlock>::iterator> > mapBlocksInFlight;


FAction *getAction(std::string identifier);
CChain *chain(const CTransaction &tx);
CChain *chain(std::string action_identifer);
CCoinsViewCache *coins(const CTransaction &tx);
FChainParams *params(std::string identifier);
boost::unordered_map<uint256, CBlockIndex*, BlockHasher> blocks(const CCoinsViewCache& inputs);


bool CheckFinalTx(const CTransaction &tx, int flags = -1);
bool CheckSequenceLocks(const CTransaction &tx, int flags);

bool CheckInputs(const CTransaction& tx, CValidationState &state, const CCoinsViewCache &view, bool fScriptChecks,
                 unsigned int flags, bool cacheStore, std::vector<CScriptCheck> *pvChecks = NULL);

/** Apply the effects of this transaction on the UTXO set represented by view */
void UpdateCoins(const CTransaction& tx, CValidationState &state, CCoinsViewCache &inputs, int nHeight);

CBlockIndex * InsertBlockIndex(std::string action_identifier, uint256 hash);


/** Check whether enough disk space is available for an incoming block */
bool CheckDiskSpace(uint64_t nAdditionalBytes = 0);
/** Open a block file (blk?????.dat) */
FILE* OpenBlockFile(std::string action_identifier, const CDiskBlockPos &pos, bool fReadOnly = false);
/** Open an undo file (rev?????.dat) */
FILE* OpenUndoFile(std::string action_identifier, const CDiskBlockPos &pos, bool fReadOnly = false);


void FindFilesToPrune(std::string action_identifier, std::set<int>& setFilesToPrune, uint64_t nPruneAfterHeight);
/** Flush all state, indexes and buffers to disk. */
//void FlushStateToDisk(std::string action_identifier);
/** Prune block files and flush state to disk. */
void UnlinkPrunedFiles(std::string action_identifier, std::set<int>& setFilesToPrune);

/** RAII wrapper for VerifyDB: Verify consistency of the block and coin databases */
class CVerifyDB {
public:
    CVerifyDB();
    ~CVerifyDB();
    bool VerifyDB(FChainParams *chainparams, CCoinsView *coinsview, int nCheckLevel, int nCheckDepth);
};

/** Convert CValidationState to a human-readable message for logging */
std::string FormatStateMessage(const CValidationState &state);

bool FindUndoPos(std::string action_identifier, CValidationState &state, int nFile, CDiskBlockPos &pos, unsigned int nAddSize);

#endif // DB_UTILS_H
